<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');

Route::post('/sendemail/send/{id}', 'SendMailController@send');
Route::post('/subscribe/send', 'SendMailController@subscribe');

Route::fallback(function() {
    return view('nopage');
});

Route::get('/products', 'PageController@productdsp');
Route::get('/product-detail/{id}', 'PageController@productdetail');
Route::post('/product-detail/{id}', 'PageController@sample');

Route::get('/blogs', 'PageController@blogHomepage');
Route::get('/blog-detail/{id}', 'PageController@blogDetail');

Route::get('blog/all', 'BlogController@index');
Route::get('blog/create', 'BlogController@create');
Route::post('blog/create', 'BlogController@store')->name('make-post.blog');
Route::get('blog/{id}', 'BlogController@show');
Route::get('blog/edit/{id}', 'BlogController@edit');
Route::post('blog/edit/{id}', 'BlogController@update')->name('update-post.blog');
Route::get('blog/destroy/{id}', 'BlogController@destroy')->name('delete-post.blog');


Route::get('blog-category/all', 'BlogcategoryController@index');
Route::get('blog-category/create', 'BlogcategoryController@create');
Route::post('blog-category/create', 'BlogcategoryController@store')->name('make.blog');
Route::get('blog-category/{id}', 'BlogcategoryController@show');
Route::get('blog-category/edit/{id}', 'BlogcategoryController@edit');
Route::post('blog-category/edit/{id}', 'BlogcategoryController@update')->name('update.blog');
Route::get('blog-category/destroy/{id}', 'BlogcategoryController@destroy')->name('delete.blog');



Route::get('/customer-info/{id}', 'CartController@index');
Route::post('/customer-info/{id}', 'CartController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/send/email/{id}', 'CartController@mail');


Route::get('productcategory', 'ProductcategoryController@index');
Route::get('productcategory/create', 'ProductcategoryController@create');
Route::post('productcategory/create', 'ProductcategoryController@store')->name('category.store');
Route::get('productcategory/{id}', 'ProductcategoryController@show');
Route::get('productcategory/edit/{id}', 'ProductcategoryController@edit');
Route::post('productcategory/edit/{id}', 'ProductcategoryController@update')->name('category.update');
Route::get('productcategory/destroy/{id}', 'ProductcategoryController@destroy')->name('category.destroy');


Route::get('product/all', 'ProductController@index');
Route::get('product/create', 'ProductController@create');
Route::post('product/create', 'ProductController@store')->name('make.product');
Route::get('product/{id}', 'ProductController@show');
Route::get('product/edit/{id}', 'ProductController@edit');
Route::post('product/edit/{id}', 'ProductController@update')->name('update.product');
Route::get('product/destroy/{id}', 'ProductController@destroy')->name('delete.product');


Route::get('team-members/all', 'TeammembersController@index');
Route::get('team-members/create', 'TeammembersController@create');
Route::post('team-members/create', 'TeammembersController@store')->name('make.team-members');
Route::get('team-members/{id}', 'TeammembersController@show');
Route::get('team-members/edit/{id}', 'TeammembersController@edit');
Route::post('team-members/edit/{id}', 'TeammembersController@update')->name('update.team-members');
Route::delete('team-members/destroy/{id}', 'TeammembersController@destroy')->name('delete.team-members');


Route::get('valuable-client/all', 'ValuableclientController@index');
Route::get('valuable-client/create', 'ValuableclientController@create');
Route::post('valuable-client/create', 'ValuableclientController@store')->name('make.valuable-client');
Route::get('valuable-client/{id}', 'ValuableclientController@show');
Route::get('valuable-client/edit/{id}', 'ValuableclientController@edit');
Route::post('valuable-client/edit/{id}', 'ValuableclientController@update')->name('update.valuable-client');
Route::get('valuable-client/destroy/{id}', 'ValuableclientController@destroy')->name('delete.valuable-client');

Route::get('/home/sliders', 'SliderController@index');
Route::get('/home/slider/create', 'SliderController@create');
Route::post('/home/slider/create', 'SliderController@store');
Route::get('/home/slider/edit/{id}', 'SliderController@edit')->name('slider.edit');
Route::post('/home/slider/edit/{id}', 'SliderController@update')->name('slider.update');
Route::delete('/home/slider/destroy/{id}', 'SliderController@destroy')->name('slider.delete');

Route::get('/home/services', 'ServicesController@index');
Route::get('/home/services/create', 'ServicesController@create');
Route::post('/home/services/create', 'ServicesController@store');
Route::get('/home/services/edit/{id}', 'ServicesController@edit')->name('service.edit');
Route::post('/home/services/edit/{id}', 'ServicesController@update')->name('service.update');
Route::delete('/home/services/destroy/{id}', 'ServicesController@destroy')->name('service.delete');