<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
    	'name','stock','description','image1','image2','price','sale_price'
    ];
}
