<?php


namespace App\Http\Controllers;

use App\Teammembers;
use App\Page;
use App\Blog;
use App\Blogcategory;
use App\slider;
use App\services;
use App\valuableclient;
use App\Product;
use App\Productcategory;
use App\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

class PageController extends Controller
{
	public function index()
	{
		$products = product::latest()->get();
        $sliders = slider::latest()->get();
        $blogs = Blog::latest()->get();
		$Services = services::latest()->get();
		$teammembers = Teammembers::latest()->get();
		$valuableclient = valuableclient::latest()->get();
		return view('welcome', compact('teammembers','valuableclient','products','blogs','sliders','Services'));
	}
	public function productdsp()
    {
        $products = product::all();
        $product_categories = productCategory::all();
        return view('products', compact('products','product_categories'));
    }
    public function productdetail($id)
    {
        $product_categories = productCategory::all();
         $product = product::findOrFail($id);
        return view('product-detail',compact('product','product_categories'));
    }
    public function sample($id, Request $request){
        $product_categories = productCategory::all();
         $product = product::findOrFail($id);
         $quantity = $request->quantity;
        return view ('cart.customer-info',compact('product','product_categories'))->with('quantity', $quantity);
    }
        public function blogDetail($id)
    {
        $blogs = Blog::findOrFail($id);
        $blogcategories = Blogcategory::all();
        return view ('blog-detail',compact('blogs','blogcategories'));
    }
    public function blogHomepage()
    {
        $blogs = Blog::all();
        $blogcategories = Blogcategory::all();
        return view ('blog',compact('blogs','blogcategories'));
    }
}
