<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use App\Productcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;


class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $carts = new cart();
         $request->validate([
            'name' => ['required', 'min:3'],
            'message' => ['required', 'min:5'],
            'price' => 'required',
            'phone' => ['required', 'min:10'],
            'landmark' => ['required', 'min:5'],
            'location' => ['required', 'min:5']
        ]);
        $carts->name = request('name');
        $carts->email = request('email');
        $carts->phone = request('phone');
        $carts->location = request('location');
        $carts->landmark = request('landmark');
        $carts->message = request('message');
        $carts->p_id = request('p_id');
        $carts->quantity = request('quantity');
        $carts->price = request('price');
        $carts->save();
        // dd($carts);
        $this->sendMail($carts->id);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        //
    }
    public function sendMail($id)
    {
        $carts = Cart::findOrFail($id);
        $sn = $carts->id;
        $name = $carts->name;
        $email = $carts->email;
        $phone = $carts->phone;
        $location = $carts->location;
        $landmark = $carts->landmark;
        $information = $carts->message;
        $p_id = $carts->p_id;
        $quantity =  $carts->quantity;
        $price =  $carts->price;
        $created_at = $carts->created_at;
        Mail::to('azizdulal.ad@gmail.com')->send(new SendMailable($name, $email, $phone, $location, $landmark, $information, $p_id, $quantity, $price,$created_at, $sn));

        return 'Order sent. Conformation Will be sent by Mail.Please check your mail in some time.';
    }
}
