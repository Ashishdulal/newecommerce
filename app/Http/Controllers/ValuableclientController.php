<?php

namespace App\Http\Controllers;

use App\valuableclient;
use Illuminate\Http\Request;

class ValuableclientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $valuableclient = valuableclient::all();
        return view('dashboard/valuable-clients/index', compact('valuableclient'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard/valuable-clients/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valuableclient = new valuableclient();
        $request->validate([
            'name' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg'
        ]);
        $valuableclient->name = $request->name;
        if (file_exists($request->file('image'))){
            $image = "valuableclient".time().'.'.$request->file('image')->getClientOriginalExtension();
            $location = public_path ('uploads');
            $request->file('image')->move($location, $image);
            $valuableclient->image = $image;
        }
        else{
            $valuableclient->image = 'placeholder.jpg';
        }
        $valuableclient->save();
        return redirect('valuable-client/all');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\valuableclient  $valuableclient
     * @return \Illuminate\Http\Response
     */
    public function show(valuableclient $valuableclient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\valuableclient  $valuableclient
     * @return \Illuminate\Http\Response
     */
    public function edit(valuableclient $valuableclient,$id)
    {
        $valuableclient = valuableclient::findOrFail($id);
        return view('dashboard/valuable-clients/edit', compact('valuableclient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\valuableclient  $valuableclient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, valuableclient $valuableclient,$id)
    {
        $valuableclient = valuableclient::findOrFail($id);
        $request->validate([
            'name' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg'
        ]);
        $valuableclient->name = $request->name;
        if (file_exists($request->file('image'))){
            $image = "valuableclient".time().'.'.$request->file('image')->getClientOriginalExtension();
            $location = public_path ('uploads');
            $request->file('image')->move($location, $image);
            $valuableclient->image = $image;
        }
        else{
            $valuableclient->image = $valuableclient->image;
        }
        $valuableclient->save();
        return redirect('valuable-client/all');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\valuableclient  $valuableclient
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $valuableclient = valuableclient::findOrFail($id)->delete();
        return redirect()->back();
    }
}
