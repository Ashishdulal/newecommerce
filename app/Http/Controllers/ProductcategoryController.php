<?php

namespace App\Http\Controllers;

use App\Productcategory;
use App\Product;
use Illuminate\Http\Request;

class ProductcategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        $product_categories = ProductCategory::all();

        return view('/dashboard/product-category/index',compact('product_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard/product-category/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title'=> ['required', 'min:3']
        ]);

        $product_categories = new ProductCategory();

        $product_categories->title = request('title');

        $product_categories->save();

        return redirect('/productcategory');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductCategory  $ProductCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $ProductCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductCategory  $ProductCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_categories = ProductCategory::findOrFail($id);

        return view('/dashboard/product-category/edit',compact('product_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductCategory  $ProductCategory
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
         $product_categories =  ProductCategory::findOrFail($id);

        $product_categories->title = request('title');

        $product_categories->save();

        return redirect ('/productcategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCategory  $ProductCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       ProductCategory::findOrFail($id)-> delete();

        return redirect('/productcategory');
    }
}
