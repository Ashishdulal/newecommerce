<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use App\Productcategory;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $carts = Cart::paginate(5);
        $product_categories = ProductCategory::all();
        return view('home', compact('carts','product_categories'));
    }
}
