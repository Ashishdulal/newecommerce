<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;	

class SendMailController extends Controller
{
    function send(Request $request)
    {
     $this->validate($request, [
      'name'     =>  'required',
      'location'     =>  'required',
      'email'  =>  'required|email',
      'phone' =>  'required',
      'landmark' =>  'required',
      'price' =>  'required',
      'message' =>  'required'
     ]);

        $data = array(
            'name'      =>  $request->name,
            'location'      =>  $request->location,
            'email'   =>   $request->email,
            'phone'   =>   $request->phone,
            'landmark'   =>   $request->landmark,
            'product_name'   =>   $request->product_name,
            'price'   =>   $request->price,
            'single_price'   =>   $request->single_price,
            'quantity'   =>   $request->quantity,
            'deliveryprice'   =>   $request->deliveryprice,
            'message'   =>   $request->message
        );
        
        $txt1 = '<html>
        <head>  
        </head>
        <body>
                    <p>Client Name: '. $data['name'] .' <br>
                    Location:'. $data['location'] .'.</p>
                    <p>Email:'. $data['email'] .'<br><br>Phone:'. $data['phone'] .'</p>
                    <p>Product:'. $data['product_name'] .'</p>
                    <p>Price:'. $data['single_price'] .'</p>
                    <p>Quantity:'. $data['quantity'] .'</p>
                    <p>Delivery Charge:<br>'. $data['deliveryprice'] .'</p>
                    <p>Total Price:'. $data['price'] .'</p>
                    <p>Message:<br>'. $data['message'] .'</p>
                    <p>It would be appriciative, if i receive the Product soon.</p>
        </body>
        </html>';       

        $to = "info@ntplbazar.com";
        $subject = "Product Inquiry";

        $headers = "From:ntplbazar.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";


        


         $result=   mail($to,$subject,$txt1,$headers);
                 return back()->with('success','Thanks for shopping with us ! Product will be delivered soon.');
        }

        function subscribe(Request $request)
    {
     $this->validate($request, [
      'email'  =>  'required|email'
     ]);

        $sdata = array(
            'email'   =>   $request->email
        );
        
        $txt2 = '<html>
        <head>  
        </head>
        <body>
                    <p>Hi, This is '. $sdata['email'] .'</p>
                    <p>I want to subscribe for the newsletter.</p>
                    <p>It would be appriciative, if i receive the subscription soon.</p>
        </body>
        </html>';       

        $to = "azizdulal.ad@gmail.com";
        $subject = "Subscription Inquiry";

        $headers = "From:Haruyosi.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";


        


         $result=   mail($to,$subject,$txt2,$headers);
                 return back()->with('success','Thanks for subscribing us!');
        }

}
