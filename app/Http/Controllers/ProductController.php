<?php

namespace App\Http\Controllers;

use App\Product;
use App\Productcategory;
use App\Cart;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = product::all();
        $product_categories = productCategory::all();

        return view('dashboard.product.index', compact('products','product_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        $arr['product_categories'] = productCategory::all();
        return view('dashboard.product.create')->with($arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $products = new product();
        $products->cat_id = request('cat_id');
        $products->name = request('name');
        $products->description = request('description');
        $products->price = request('price');
        $products->sale_price = request('sale_price');
        $products->stock = request('stock');
        $request->validate([
            'name' => ['required', 'min:3'],
            'description' => ['required', 'min:10'],
            'sale_price' => 'required',
            'image1' => 'image|mimes:jpg,png,jpeg|',
            'image2' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $products->name = $request->name;
        if(file_exists($request->file('image1'))){
            $image1 = "products".time().'.'.$request->file('image1')->getclientOriginalName();
            $location = public_path('uploads');
            $request->file('image1')->move($location, $image1);
            $products->image1 = $image1;
        }
        else{
            $products->image1 = 'Placeholder.jpg';
        }
        if(file_exists($request->file('image2'))){
            $image2 = "products".time().'.'.$request->file('image2')->getclientOriginalName();
            $location = public_path('uploads');
            $request->file('image2')->move($location, $image2);
            $products->image2 = $image2;
        }
        else{
            $products->image2 = 'Placeholder.jpg';
        }
        $products->save();
        return redirect('/product/all');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_categories = productCategory::all();
         $products = product::findOrFail($id);

        return view ('dashboard.product.edit', compact('products','product_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, product $product,$id)
    {
       $products = product::findOrFail($id);
       $request->validate([
            'name' => ['required', 'min:3'],
            'description' => ['required', 'min:10'],
            'sale_price' => 'required',
            'image1' => 'image|mimes:jpg,png,jpeg|',
            'image2' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $products->cat_id = request('cat_id');
        $products->name = request('name');
        $products->description = request('description');
        $products->price = request('price');
        $products->sale_price = request('sale_price');
        $products->stock = request('stock');
        $products->name = $request->name;
        if(file_exists($request->file('image1'))){
            $image1 = "products".time().'.'.$request->file('image1')->getclientOriginalName();
            $location = public_path('uploads');
            $request->file('image1')->move($location, $image1);
            $products->image1 = $image1;
        }
        else{
            $products->image1 = $products->image1;
        }
        if(file_exists($request->file('image2'))){
            $image2 = "products".time().'.'.$request->file('image2')->getclientOriginalName();
            $location = public_path('uploads');
            $request->file('image2')->move($location, $image2);
            $products->image2 = $image2;
        }
        else{
            $products->image2 = $products->image2;
        }
        $products->save();
        return redirect('/product/all');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $products = product::findOrFail($id)-> delete();
        return back();

    }
}