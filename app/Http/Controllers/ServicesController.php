<?php

namespace App\Http\Controllers;

use App\services;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = services::all();
        return view ('dashboard.services.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $services = new services();
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $services->name = $request->name;
        $services->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "services".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $services->image = $image;
        }
        else{
            $services->image = 'placeholder.jpg';
        }        
        $services->save();
        return redirect('/home/services');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\services  $services
     * @return \Illuminate\Http\Response
     */
    public function show(services $services)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\services  $services
     * @return \Illuminate\Http\Response
     */
    public function edit(services $services,$id)
    {
        $services = services::findOrFail($id);
        return view ('dashboard.services.edit',compact('services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\services  $services
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, services $services,$id)
    {
        $services = services::findOrFail($id);
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $services->name = $request->name;
        $services->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "services".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $services->image = $image;
        }
        else{
            $services->image = $services->image;
        }        
        $services->save();
        return redirect('/home/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\services  $services
     * @return \Illuminate\Http\Response
     */
    public function destroy( services $services,$id)
    {
        $services = services::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
