<?php

namespace App\Http\Controllers;

use App\Teammembers;
use Illuminate\Http\Request;

class TeammembersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teammember = teammembers::all();
        return view('dashboard.team.index', compact('teammember'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $teammember = new teammembers();
        $request->validate([
            'name' => 'required',
            'designation' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg'
        ]);
        $teammember->name =$request->name;
        $teammember->designation =$request->designation;
        $teammember->description =$request->description;
        $teammember->facebook_link =$request->facebook_link;
        $teammember->twitter_link =$request->twitter_link;
        $teammember->instagram_link =$request->instagram_link;
        $teammember->Youtube_link =$request->Youtube_link;
        if(file_exists($request->file('image'))){
            $image = "teammember".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $teammember->image = $image;
        }
        else{
            $teammember->image = 'placeholder.jpg';
        }
        $teammember->save();
        return redirect('team-members/all');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Teammembers  $teammembers
     * @return \Illuminate\Http\Response
     */
    public function show(Teammembers $teammembers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Teammembers  $teammembers
     * @return \Illuminate\Http\Response
     */
    public function edit(Teammembers $teammembers, $id)
    {
        $teammember = teammembers::findOrFail($id);
        return view ('dashboard.team.edit',compact('teammember'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teammembers  $teammembers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teammembers $teammembers,$id)
    {
        $teammember = teammembers::findOrFail($id);
        $request->validate([
            'name' => 'required',
            'designation' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg'
        ]);
        $teammember->name =$request->name;
        $teammember->designation =$request->designation;
        $teammember->description =$request->description;
        $teammember->facebook_link =$request->facebook_link;
        $teammember->twitter_link =$request->twitter_link;
        $teammember->instagram_link =$request->instagram_link;
        $teammember->Youtube_link =$request->Youtube_link;
        if(file_exists($request->file('image'))){
            $image = "teammember".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $teammember->image = $image;
        }
        else{
            $teammember->image = $teammember->image;
        }
        $teammember->save();
        return redirect('team-members/all');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Teammembers  $teammembers
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $teammember = teammembers::findOrFail($id)->delete();
    return redirect()->back();
    }
}
