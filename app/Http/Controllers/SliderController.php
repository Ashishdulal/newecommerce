<?php

namespace App\Http\Controllers;

use App\slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = slider::latest()->get();
        return view('/dashboard/slider/index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('/dashboard/slider/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slider = new slider();
        $request->validate([
            'name' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg'
        ]);
        $slider->name = $request->name;
        $slider->title = $request->title;
        $slider->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "Slider".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $slider->image = $image;
        }
        else{
            $slider->image = 'placeholder.jpg';
        }        
        $slider->save();
        return redirect('/home/sliders');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(slider $slider,$id)
    {
        $slider = slider::findOrFail($id);
        return view('/dashboard/slider/edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, slider $slider,$id)
    {
        $slider = slider::findOrFail($id);

        $request->validate([
            'name' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $slider->name = $request->name;
        $slider->description = $request->description;
        $slider->title = $request->title;
        if(file_exists($request->file('image'))){
            $image = "Slider".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $slider->image = $image;
        }
        else{
            $slider->image = $slider->image;
        }        
        $slider->save();
        return redirect('/home/sliders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = slider::findOrFail($id) ->delete();
        return back();
    }
}
