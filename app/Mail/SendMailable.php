<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $created_at;
    public $email;
    public $phone;
    public $location;
    public $landmark;
    public $information;
    public $p_id;
    public $quantity;
    public $price;
    public $sn;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $phone, $location, $landmark, $information, $p_id, $quantity, $price, $created_at, $sn)
    {
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->location = $location;
        $this->landmark = $landmark;
        $this->message = $information;
        $this->p_id = $p_id;
        $this->quantity = $quantity;
        $this->price = $price;
        $this->created_at = $created_at;
        $this->sn = $sn;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.name');
    }
}
