<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teammembers extends Model
{
    protected $fillable =[
    	'name','designation','image','description','facebook_link','twitter_link',
    	'instagram_link','youtube_link'
    ];
}
