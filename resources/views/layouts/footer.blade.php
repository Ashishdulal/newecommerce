    <!--footer starts-->     
    <footer>
      <div class="copyright">
        <!--container starts-->
        <div class="container">

          <div id="shopify-section-footer" class="shopify-section index-section">
            <div data-section-id="footer" data-section-type="footer-section">
              <!--footer starts-->     
              <footer>
                <div class="copyright">
                  <!--container starts-->
                  <div class="container">
                    
                    <p> &copy; NepCommerce 2019  | All Rights Reserved | Powered by <a href="http://www.nepgeeks.com" target="_blank"><span>Nepgeeks</span></a></p>
                    

                    
                </div>
                <!--container ends-->
            </div>
        </footer>
        <!--footer ends-->
    </div>

</div>

</div>
<!--wrapper ends-->

<script src="/assets/themes_support/api.jquery-e94e010e92e659b566dbc436fdfe5242764380e00398907a14955ba301a4749f.js" type="text/javascript"></script>




<script src="/assets/jquery.carouFredSel-6.2.1-packed.js?0" type="text/javascript"></script>   
<script src="/assets/jquery.isotope.min.js?0" type="text/javascript"></script>
<script src="/assets/jquery.animateNumber.min.js?0" type="text/javascript"></script>   
<script src="/assets/jquery.inview.js?0" type="text/javascript"></script>
<script src="/assets/jquery.scrollTo.js?0" type="text/javascript"></script>    
<script src="/assets/jquery.themepunch.tools.min.js?0" type="text/javascript"></script>
<script src="/assets/jquery.themepunch.revolution.min.js?0" type="text/javascript"></script>
<script type="text/javascript">
  jQuery.noConflict();
  jQuery(document).ready(function($) {
    $('.tp-banner').show().revolution(
    {
        dottedOverlay:"none",
        delay:9000,
        startwidth:1160,
        startheight:900,
        hideThumbs:200,

        thumbWidth:100,
        thumbHeight:50,
        thumbAmount:3,

        navigationType:"none",
        navigationArrows:"solo",
        navigationStyle:"round",

        touchenabled:"on",
        onHoverStop:"off",

        swipe_velocity: 0.7,
        swipe_min_touches: 1,
        swipe_max_touches: 1,
        drag_block_vertical: false,


        keyboardNavigation:"off",

        navigationHAlign:"center",
        navigationVAlign:"bottom",
        navigationHOffset:0,
        navigationVOffset:20,

        soloArrowLeftHalign:"left",
        soloArrowLeftValign:"center",
        soloArrowLeftHOffset:20,
        soloArrowLeftVOffset:0,

        soloArrowRightHalign:"right",
        soloArrowRightValign:"center",
        soloArrowRightHOffset:20,
        soloArrowRightVOffset:0,

        shadow:0,
        fullWidth:"off",
        fullScreen:"on",

        spinner:"spinner0",

        stopLoop:"off",
        stopAfterLoops:-1,
        stopAtSlide:-1,

        shuffle:"off",


        forceFullWidth:"off",
        fullScreenAlignForce:"off",
        minFullScreenHeight:"",
        hideTimerBar:"on",
        hideThumbsOnMobile:"off",
        hideNavDelayOnMobile:1500,
        hideBulletsOnMobile:"off",
        hideArrowsOnMobile:"off",
        hideThumbsUnderResolution:0,

        fullScreenOffsetContainer: "",
        fullScreenOffset: "",
        hideSliderAtLimit:0,
        hideCaptionAtLimit:0,
        hideAllCaptionAtLilmit:0,
        startWithSlide:0                
    });

          });   //ready

      </script>

      <script src="/assets/jquery.donutchart.js?0" type="text/javascript"></script>    
      <script src="/assets/jquery.nav.js?0" type="text/javascript"></script>    

      <script src="/assets/jquery.bxslider.min.js?0" type="text/javascript"></script>    
      <script src="/assets/jquery.parallax-1.1.3.js?0" type="text/javascript"></script>    
      <script src="/assets/jquery-easing-1.3.js?0" type="text/javascript"></script>    
      <script src="/assets/jquery.jcarousel.min.js?0" type="text/javascript"></script>    
      <script src="/assets/jquery.prettyPhoto.js?0" type="text/javascript"></script>    
      <script src="/assets/masonry.pkgd.min.js?0" type="text/javascript"></script>    
      <script src="/assets/responsive-nav.js?0" type="text/javascript"></script>
      <script src="/assets/jquery.meanmenu.min.js?0" type="text/javascript"></script>    

      <!-- **Sticky Nav** -->
      <script src="/assets/jquery.sticky.js?0" type="text/javascript"></script>

      <!-- Style Picker Starts -->
      <script src="/assets/jquery.cookie.js?0" type="text/javascript"></script>

      <!-- Style Picker ends -->

      <!-- **To Top** -->
      <script src="/assets/jquery.ui.totop.min.js?0" type="text/javascript"></script>    
      <script src="/assets/jquery.tweet.min.js?0" type="text/javascript"></script>    
      <script src="/assets/greensock.js?0" type="text/javascript"></script>
      <script src="/assets/ScrollToPlugin.min.js?0" type="text/javascript"></script>
      <script src="/assets/smoothPageScroll.js?0" type="text/javascript"></script>
      <script src="/assets/jquery.mobilemenu.js?0" type="text/javascript"></script>
      <script src="/assets/custom.js?0" type="text/javascript"></script>  
      <script src="/assets/scripts.js?0" type="text/javascript"></script>
      <script src="/assets/jquery.zoom.js?0" type="text/javascript"></script>
      <script src="/assets/fancybox.js?0" type="text/javascript"></script>




    
