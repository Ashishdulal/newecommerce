    <div class="sidebar" data-color="orange">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
      -->
      <div class="logo">
        <a href="/" class="simple-text logo-mini">
          Nep
        </a>
        <a href="/" class="simple-text logo-normal">
         Commerce
        </a>
      </div>
      <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
          <li class="active ">
            <a href="/login">
              <i class="now-ui-icons design_app"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li>
            <a href="/home/sliders">
              <i class="now-ui-icons education_atom"></i>
              <p>Sliders</p>
            </a>
          </li>
          <li>
            <a class="dropdown" data-toggle="dropdown">
              <i class="now-ui-icons design_bullet-list-67"></i>
              <p>Product Category</p>
              <div class="dropdown-menu float-right" role="menu">
                <a href="/productcategory" class="dropdown-item">View Categories</a>
                <a href="/productcategory/create" class="dropdown-item">Add new Category</a>
                <!--  <a href="#" class="dropdown-item">Sub Category</a> -->
              </div>
            </a>
          </li>
          <li>
            <a class="dropdown" data-toggle="dropdown">
              <i class="now-ui-icons design_bullet-list-67"></i>
              <p>Products</p>
              <div class="dropdown-menu float-right" role="menu">
                <a href="/product/all" class="dropdown-item">View Products</a>
                <a href="/product/create" class="dropdown-item">Add new Product</a>
                <!--  <a href="#" class="dropdown-item">Sub product</a> -->
              </div>
            </a>
          </li>
          <li>
            <a class="dropdown" data-toggle="dropdown">
          <i class="now-ui-icons media-1_album"></i>
              <p>Blog</p>
              <div class="dropdown-menu float-right" role="menu">
                <a href="/blog-category/all" class="dropdown-item">Blog Category</a>
                <div class="dropdown-divider"></div>
                <a href="/blog/all" class="dropdown-item">Blogs</a>
                </div>
            </a>
          </li>
          <li>
            <a href="/team-members/all">
              <i class="now-ui-icons users_single-02"></i>
              <p>Team Members</p>
            </a>
          </li>
          <li>
            <a href="/valuable-client/all">
              <i class="now-ui-icons users_single-02"></i>
              <p>Valuable Clients</p>
            </a>
          </li>
          
          <li>
            <a href="/home/services">
              <i class="now-ui-icons location_map-big"></i>
              <p>Services</p>
            </a>
          </li>
  <!--         
          <li>
            <a href="">
              <i class="now-ui-icons ui-1_bell-53"></i>
              <p>Notifications</p>
            </a>
          </li> -->
          
          <li class="active-pro">
            <a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            <i class="now-ui-icons arrows-1_cloud-download-93"></i>
            {{ __('Logout') }}
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </li>
      </ul>
    </div>
  </div>