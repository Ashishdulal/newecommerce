<!doctype html>
<!--[if lt IE 7]><html class="no-js ie6 oldie" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js ie7 oldie" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js ie8 oldie" lang="en"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en"><!--<![endif]-->
<head>

<link rel="shortcut icon" href="/images/favicon.png?0" type="image/png" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

<meta charset="utf-8" />
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' /><![endif]-->


<!--[if gte IE 9]><!-->
  <link rel="stylesheet" media="all" href="/assets/v2-ltr-edge-b2ba0c53726960f6205774d4a0a6af40-11609.css" />

<!--<![endif]-->

<title>
 Nep-Commerce
</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>

  $(document).ready(function(){
    $.noConflict();
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      // event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 1000, function(){
       
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>

<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- **CSS - stylesheets** -->
<link href="/assets/style.css?0" rel="stylesheet" type="text/css" media="all" />
<link href="/assets/color.css?0" rel="stylesheet" type="text/css" media="all" />
<link href="/assets/shortcodes.css?0" rel="stylesheet" type="text/css" media="all" />
<link href="/assets/skin.css?0" rel="stylesheet" type="text/css" media="all" />
<link href="/css/mystyle.css" rel="stylesheet" type="text/css" media="all" />


<!-- **Additional - stylesheets** -->
<link href="/assets/responsive.css?0" rel="stylesheet" type="text/css" media="all" />
<link href="/assets/meanmenu.css?0" rel="stylesheet" type="text/css" media="all" />
<link href="/assets/settings.css?0" rel="stylesheet" type="text/css" media="all" />
<link href="/assets/prettyPhoto.css?0" rel="stylesheet" type="text/css" media="all" />



<!-- **Font Awesome** -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<!--**Google Fonts**-->

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic">



<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Alegreya+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">



<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Arvo">





<link rel="stylesheet" media="screen" href="/compiled_assets/styles.css?0">


<!-- **jQuery** -->  
<script src="/assets/jquery-1.10.2.min.js?0" type="text/javascript"></script>
<script src="/assets/jquery.tabs.min.js?0" type="text/javascript"></script>
<script src="/assets/jquery-migrate-1.2.1.js?0" type="text/javascript"></script>
<script src="/assets/themes_support/option_selection-fe6b72c2bbdd3369ac0bfefe8648e3c889efca213baefd4cfb0dd9363563831f.js" type="text/javascript"></script>  
<script src="/assets/modernizr-2.6.2.min.js?0" type="text/javascript"></script>
<!-- **jQuery** -->
</head>
<body>
  <!--header starts-->


  <div id="shopify-section-header" class="shopify-section index-section">
    <div data-section-id="header" data-section-type="header-model">  
      <!--header starts-->
      <header id="header">
        <!--container starts-->
        <div class="container">
          <!--logo starts-->
          <div id="logo">
           
            <a href="/"><img src="/images/ntpl-bazar.png" alt="Momos" /></a>            
            
          </div>
          <!--logo ends-->
            <div id="menu-container">
              <!--nav starts-->
              <nav id="main-menu">
                <ul class="group menu">
                  
                  
                  <li>
                    <a href="/" class=" current_page_item">
                      <span>Home</span></a>
                    </li>


                    <li>
                      <a href="/#section_2" class=" current_page_item">
                        <span>Shop</span></a>
                      </li>
                      
                      
                      
                      <li>
                        <a href="/#infoabout" class="">
                          <span>About</span></a>
                        </li>
                        
                        
                        
                        <li>
                          <a href="/#section_3" class="">
                            <span>Services</span></a>
                          </li>


                          <li>
                            <a href="/#section_7" class="">
                              <span>Blog</span></a>
                            </li>
                            <li>
                              <a href="/#section_8" class="">
                                <span>Contact Us</span></a>
                              </li>
                              
                              
  <!-- <li class="menu-item-simple-parent">
    <a href="/products" class="">
      <span>Shop</span>
    </a>
    <ul class="sub-menu">
      
      <li><a href="/collections/all" class="">With Sidebar</a></li>
      
      <li><a href="/collections/collection-wide" class="">Without Sidebar</a></li>
      
    </ul>
  </li> -->
  
  
  
</ul>

</nav>              
<!--nav ends-->

</div>
</div>
<!--container ends-->      

<!-- <div class="customer-links">
  <span class="customer-login">
    
    <a class="gomd" href="/home"><span class="fa fa-sign-in"></span> Sign in </a>|
    
    <a href="/register">Register</a>
    
    
  </span>
</div> -->

</header>
<!--header ends-->
</div>
</div>

    <!--header ends-->