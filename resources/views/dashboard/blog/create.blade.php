@extends('layouts.admin.index')


@section('content')

<div class="main-panel ps" id="main-panel">
	<nav class="navbar navbar-expand-lg  bg-primary ">
		<div class="container-fluid">
			<div class="navbar-wrapper">
				<div class="navbar-toggle">
					<button type="button" class="navbar-toggler">
						<span class="navbar-toggler-bar bar1"></span>
						<span class="navbar-toggler-bar bar2"></span>
						<span class="navbar-toggler-bar bar3"></span>
					</button>
				</div>
				<a class="navbar-brand" href="/login">Dashboard</a>
			</div>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-bar navbar-kebab"></span>
				<span class="navbar-toggler-bar navbar-kebab"></span>
				<span class="navbar-toggler-bar navbar-kebab"></span>
			</button>
			<div class="collapse navbar-collapse justify-content-end" id="navigation">
				<form>
					<div class="input-group no-border">
						<input type="text" value="" class="form-control" placeholder="Search...">
						<div class="input-group-append">
							<div class="input-group-text">
								<i class="now-ui-icons ui-1_zoom-bold"></i>
							</div>
						</div>
					</div>
				</form>
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="#pablo">
							<i class="now-ui-icons media-2_sound-wave"></i>
							<p>
								<span class="d-lg-none d-md-block">Stats</span>
							</p>
						</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="now-ui-icons location_world"></i>
							<p>
								<span class="d-lg-none d-md-block">Some Actions</span>
							</p>
						</a>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#pablo">
							<i class="now-ui-icons users_single-02"></i>
							<p>
								<span class="d-lg-none d-md-block">Account</span>
							</p>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container gap-maker">
		<div  class="card ">
			<div  class="card-header ">
				<h4  class="card-title">Create Team Member</h4>
			</div>
			<div  class="card-body ">
				@if($errors->any())
				@foreach($errors->all() as $error)
				<ul>
					<li>{{$error}}</li>
				</ul>
				@endforeach
				@endif

				<form class="form-horizontal" action="/blog/create" method="post" enctype="multipart/form-data">
				@csrf
				<fieldset>
					<!-- Post Title input-->
					<div class="form-group">
						<label class="col-md-3 control-label" for="title">Post Title:</label>
						<div class="col-md-9">
							<input id="title" name="title" type="text" required="" placeholder="Enter the Post Title" class="form-control">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label" for="image">Choose Post Category:</label>
						<div class="col-md-9">
						<select name="cat_id" required class="form-control">

							<option value="">---</option>
							@foreach($blogcategories as $blog)

							<option value="{{$blog->id}}">{{$blog->title}}</option>

							@endforeach
						</select>
					</div>
					</div>

					<!-- image input-->
					<div class="form-group">
						<label for="image">Select The Product Image:</label>
						<div class="row">
					<div  class="col-md-6">
						<app-image-upload  >
						<div  class="fileinput text-center">
							<div  class="thumbnail img-raised">
								<img  alt="..." src="/uploads/placeholder.jpg">
							</div>
							<div >
								<!----><button name="f_image"  class="btn btn-raised btn-round btn-default btn-file ng-star-inserted"> Select image </button><!---->
								<!---->
							</div>
								<input  type="file" name="f_image"  accept="image/png, image/jpg, image/jpeg">
							</div>
						</app-image-upload>
					</div>
					<div  class="col-md-6">
						<app-image-upload  >
						<div  class="fileinput text-center">
							<div  class="thumbnail img-raised">
								<img  alt="..." src="/uploads/placeholder.jpg">
							</div>
							<div >
								<!----><button name="i_image"  class="btn btn-raised btn-round btn-default btn-file ng-star-inserted"> Select image </button><!---->
								<!---->
							</div>
							<input  type="file" name="i_image"  accept="image/png, image/jpg, image/jpeg">
							</div>
						</app-image-upload>
					</div>
					</div>
				</div>                
					<div class="form-group">
						<label class="col-md-3 control-label" for="image">Description:</label>
						<div class="col-md-9">
							<textarea id="description" name="description" type="text" required="" placeholder="Post description" class="form-control"></textarea>
						</div>
					</div>

					<!-- Form actions -->
					<div class="form-group">
						<div class="col-md-12 widget-right">
							<button type="submit" class="btn btn-default btn-md pull-right">Submit</button>
						</div>
					</div>
				</fieldset>
			</form>

			</div>
		</div>
	</div>
 
	@endsection