@extends('layouts.admin.index')


@section('content')

<div class="main-panel ps" id="main-panel">
	<nav class="navbar navbar-expand-lg  bg-primary ">
		<div class="container-fluid">
			<div class="navbar-wrapper">
				<div class="navbar-toggle">
					<button type="button" class="navbar-toggler">
						<span class="navbar-toggler-bar bar1"></span>
						<span class="navbar-toggler-bar bar2"></span>
						<span class="navbar-toggler-bar bar3"></span>
					</button>
				</div>
				<a class="navbar-brand" href="/login">Dashboard</a>
			</div>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-bar navbar-kebab"></span>
				<span class="navbar-toggler-bar navbar-kebab"></span>
				<span class="navbar-toggler-bar navbar-kebab"></span>
			</button>
			<div class="collapse navbar-collapse justify-content-end" id="navigation">
				<form>
					<div class="input-group no-border">
						<input type="text" value="" class="form-control" placeholder="Search...">
						<div class="input-group-append">
							<div class="input-group-text">
								<i class="now-ui-icons ui-1_zoom-bold"></i>
							</div>
						</div>
					</div>
				</form>
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="#pablo">
							<i class="now-ui-icons media-2_sound-wave"></i>
							<p>
								<span class="d-lg-none d-md-block">Stats</span>
							</p>
						</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="now-ui-icons location_world"></i>
							<p>
								<span class="d-lg-none d-md-block">Some Actions</span>
							</p>
						</a>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#pablo">
							<i class="now-ui-icons users_single-02"></i>
							<p>
								<span class="d-lg-none d-md-block">Account</span>
							</p>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container gap-maker">
		<div  class="card ">
			<div  class="card-header ">
				<h4  class="card-title">All Valuable Clients</h4>
				<a href="/valuable-client/create" class="href"><button type="submit" class="btn btn-primary">Add New client</button></a>
			</div>
			<div  class="card-body ">

				<table class="table table-hover">

					<thead>
						<tr>
							<th>Id</th>
							<th>Organization Name</th>
							<th>Image</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($valuableclient as $client)
						<tr>
							<td>{{$client->id}}.</td>
							<td>{{$client->name}}</td>
							<td class="work-img">
								<img src="/uploads/{{$client->image}}" alt="{{$client->name}}">
							</td>
							<td>
								<a href="/valuable-client/edit/{{$client->id}}" class="btn btn-primary">
								Edit
							</a>|
								<form action="{{route('delete.valuable-client',$client->id)}}">
									<button type="submit" class="btn btn-danger" onclick="confirmDelete(event)">Delete</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		function confirmDelete(evt){		
			let result = confirm("Are you sure?");
			if(! result){
				evt.stopPropagation();
				evt.preventDefault();	
			}
		}
	</script>

	@endsection