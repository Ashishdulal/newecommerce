@extends('layouts.admin.index')


@section('content')

<div class="main-panel ps" id="main-panel">
	<nav class="navbar navbar-expand-lg  bg-primary ">
		<div class="container-fluid">
			<div class="navbar-wrapper">
				<div class="navbar-toggle">
					<button type="button" class="navbar-toggler">
						<span class="navbar-toggler-bar bar1"></span>
						<span class="navbar-toggler-bar bar2"></span>
						<span class="navbar-toggler-bar bar3"></span>
					</button>
				</div>
				<a class="navbar-brand" href="/login">Dashboard</a>
			</div>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-bar navbar-kebab"></span>
				<span class="navbar-toggler-bar navbar-kebab"></span>
				<span class="navbar-toggler-bar navbar-kebab"></span>
			</button>
			<div class="collapse navbar-collapse justify-content-end" id="navigation">
				<form>
					<div class="input-group no-border">
						<input type="text" value="" class="form-control" placeholder="Search...">
						<div class="input-group-append">
							<div class="input-group-text">
								<i class="now-ui-icons ui-1_zoom-bold"></i>
							</div>
						</div>
					</div>
				</form>
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="#pablo">
							<i class="now-ui-icons media-2_sound-wave"></i>
							<p>
								<span class="d-lg-none d-md-block">Stats</span>
							</p>
						</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="now-ui-icons location_world"></i>
							<p>
								<span class="d-lg-none d-md-block">Some Actions</span>
							</p>
						</a>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#pablo">
							<i class="now-ui-icons users_single-02"></i>
							<p>
								<span class="d-lg-none d-md-block">Account</span>
							</p>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container gap-maker">
		<div  class="card ">
			<div  class="card-header ">
				<h4  class="card-title">Edit Team Member</h4>
			</div>
			<div  class="card-body ">
				@if($errors->any())
				@foreach($errors->all() as $error)
				<ul>
					<li>{{$error}}</li>
				</ul>
				@endforeach
				@endif

				<form class="form-horizontal" action="/team-members/edit/{{ $teammember->id}}" method="post" enctype="multipart/form-data">
					@csrf
					<fieldset>
						<!-- Name input-->
						<div class="form-group">
							<label class="col-md-3 control-label" for="name">Member Name:</label>
							<div class="col-md-9">
								<input id="name" name="name" type="text" value="{{$teammember->name}}"  class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="designation">Member Designation:</label>
							<div class="col-md-9">
								<input id="designation" name="designation" type="text" value="{{$teammember->designation}}"  class="form-control">
							</div>
						</div>						
						<div class="form-group">
							<label class="col-md-3 control-label" for="image">Description:</label>
							<div class="col-md-9">
								<textarea id="description" name="description" type="text" required=""  class="form-control">{{$teammember->description}}</textarea>
							</div>
						</div>
						<!-- image input-->
						<div class="form-group col-md-6">
							<app-image-upload  >
						<div  class="fileinput text-center">
							<div  class="thumbnail img-raised">
								<img src="/uploads/{{$teammember->image}}" alt="{{$teammember->name }}"/>
							</div>
							<div >
								<!----><button  class="btn btn-raised btn-round btn-default btn-file ng-star-inserted"> Select image </button><!---->
								<!----></div><input  type="file" name="image"  accept="image/png, image/jpg, image/jpeg">
							</div>
						</app-image-upload>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="facebook_link">Facebook Link:</label>
							<div class="col-md-9">
								<input id="facebook_link" name="facebook_link" type="text" value="{{$teammember->facebook_link}}" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="twitter_link">Twitter Link:</label>
							<div class="col-md-9">
								<input id="twitter_link" name="twitter_link" type="text" required="" value="{{$teammember->twitter_link}}" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="instagram_link">Instagram Link:</label>
							<div class="col-md-9">
								<input id="instagram_link" name="instagram_link" type="text" required="" value="{{$teammember->instagram_link}}" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="Youtube_link">Youtube Link:</label>
							<div class="col-md-9">
								<input id="Youtube_link" name="Youtube_link" type="text" required="" value="{{$teammember->Youtube_link}}" class="form-control">
							</div>
						</div>

						<!-- Form actions -->
						<div class="form-group">
							<div class="col-md-12 widget-right">
								<button type="submit" class="btn btn-default btn-md pull-right">Submit</button>
							</div>
						</div>
					</fieldset>
				</form>

			</div>
		</div>
	</div>

	@endsection