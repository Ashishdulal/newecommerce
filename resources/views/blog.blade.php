@include('layouts.header')

   <!--main-content starts-->
    <div id="main-content">
      
<div class="page-title page-title-bg  subtitle-for-single-product">	<div class="page-title-container">		
      
      <h2>News</h2>
      
      
    </div>
   </div>
	<div class="dt-sc-margin70"></div>
 


<div class="container">
  <section class="with-sidebar with-right-sidebar" id="primary">
    @foreach ($blogs as $blog)
    <div class="dt-sc-one-half column ">
      <article class="blog-entry">    
        
        <div class="entry-thumb">
          <a class="article__featured-image" href="/blog-detail/{{$blog->id}}">
            <img src="/uploads/{{$blog->f_image}}" alt="ALONE IN THE BACKYARD" />
          </a>
        </div>		
        
        <div class="entry-details entry-move-right">
          <div class="entry-title">
            <span class="hexagon">
              <span class="corner1"></span>
              <span class="corner2"></span>
              <i class="fa fa-pencil"></i>
            </span>
            <h4><a href="/blog-detail/{{$blog->id}}"> {{$blog->title}}</a> </h4>
          </div>
          <div class="entry-body">
            <p><p>{{$blog->description}}</p>
          </div>
          <div class="entry-metadata">
            <h6 class="date"><span class="hexagon2">
              <span class="corner1"></span>
              <span class="corner2"></span><i class="fa fa-calendar"></i></span> 
             {{$blog->created_at}}</h6>
            <h6 class="author"> <span class="hexagon2">
              <span class="corner1"></span>
              <span class="corner2"></span><i class="fa fa-user"></i></span> <a href="/blogs/news/18477647-alone-in-the-backyard" title="ram"> Admin </a> </h6>
          </div>
        </div>
      </article>
    </div>
    @endforeach

    </section>
  <!-- Begin sidebar -->
  <div id="secondary">
  
  <aside class="widget">
    
    <h4 class="widgettitle">  Recent Posts  </h4>
    
    <div class="clear"></div>
    <ul class="recent-posts-widget">
      
      <li> 
        
        <a class="article__featured-image" href="/blog-detail">
          <img src="//cdn.shopify.com/s/files/1/0687/0339/articles/blog5_1024x1024_1_medium.jpg?v=1475494657" alt="ALONE IN THE BACKYARD" />
        </a>
        
        <div class="entry-details">
          <div class="entry-title"><h5> <a href="/blogs/news/18477647-alone-in-the-backyard">ALONE IN THE BACKYARD</a> </h5></div>
          <div class="entry-metadata">
            <h6 class="date"> <span class="hexagon2">
              <span class="corner1"></span>
              <span class="corner2"></span> <i class="fa fa-calendar"></i></span> 24 Dec 2014 </h6>
          </div>
          <!--entry-metadata ends-->
        </div>                        
      </li>
      
      <li> 
        
        <a class="article__featured-image" href="/blog-detail">
          <img src="//cdn.shopify.com/s/files/1/0687/0339/articles/blog4_9b9ad42b-ce27-41ce-bf6c-26c027187c3b_1024x1024_1_medium.jpg?v=1475494625" alt="MONSOON PHOTOGRAPHY" />
        </a>
        
        <div class="entry-details">
          <div class="entry-title"><h5> <a href="/blogs/news/18477531-monsoon-photography">MONSOON PHOTOGRAPHY</a> </h5></div>
          <div class="entry-metadata">
            <h6 class="date"> <span class="hexagon2">
              <span class="corner1"></span>
              <span class="corner2"></span> <i class="fa fa-calendar"></i></span> 24 Dec 2014 </h6>
          </div>
          <!--entry-metadata ends-->
        </div>                        
      </li>
      
      <li> 
        
        <a class="article__featured-image" href="/blog-detail">
          <img src="/images/blog6_1024x1024_1_medium.jpg?v=1475494583" alt="YOUTUBE – CONTRARY TO POPULAR" />
        </a>
        
        <div class="entry-details">
          <div class="entry-title"><h5> <a href="/blogs/news/18143843-youtube-contrary-to-popular">YOUTUBE – CONTRARY TO POPULAR</a> </h5></div>
          <div class="entry-metadata">
            <h6 class="date"> <span class="hexagon2">
              <span class="corner1"></span>
              <span class="corner2"></span> <i class="fa fa-calendar"></i></span> 18 Nov 2014 </h6>
          </div>
          <!--entry-metadata ends-->
        </div>                        
      </li>
      
      <li> 
        
        <a class="article__featured-image" href="/blog-detail">
          <img src="//cdn.shopify.com/s/files/1/0687/0339/articles/blog_1024x1024_1_medium.jpg?v=1475494993" alt="VIMEO – NAM MALESUADA AUGUE" />
        </a>
        
        <div class="entry-details">
          <div class="entry-title"><h5> <a href="/blogs/news/18143839-vimeo-nam-malesuada-augue">VIMEO – NAM MALESUADA AUGUE</a> </h5></div>
          <div class="entry-metadata">
            <h6 class="date"> <span class="hexagon2">
              <span class="corner1"></span>
              <span class="corner2"></span> <i class="fa fa-calendar"></i></span> 18 Nov 2014 </h6>
          </div>
          <!--entry-metadata ends-->
        </div>                        
      </li>
      
    </ul>
  </aside>
  
  
  
</div>
<!-- secondary ends -->
<div class="dt-sc-margin20"></div>
  <!-- End sidebar -->
</div>


<div class="dt-sc-margin50"></div>
<div class="dt-sc-margin10"></div>
    </div>
    <!--main-content ends-->

    @include('layouts.footer')