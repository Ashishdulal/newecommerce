@extends('layouts.app')

@section('content')

 <!--main-content starts-->
    <div id="main-content">
      
<div class="page-title page-title-bg  subtitle-for-single-product">	<div class="page-title-container">		
      
      <h2>{{$blogs->title}}</h2>
      

      
    </div>
   </div>
	<div class="dt-sc-margin70"></div>
 


<!-- Container starts-->
<div class="container">
  <div id="primary" class="with-right-sidebar with-sidebar">     
    <article class="blog-entry">
      <!--entry-thumb starts-->
       
        <div class="entry-thumb">
          <div class="col-sm-6 image_height_blog">
          <a class="article__featured-image" href="/uploads/{{$blogs->f_image}}">
            <img src="/uploads/{{$blogs->f_image}}" alt="{{$blogs->name}}" />
          </a>
        </div>
        <div class="col-sm-6 image_height_blog">
          <a class="article__featured-image" href="/uploads/{{$blogs->i_image}}">
          <img src="/uploads/{{$blogs->i_image}}" alt="{{$blogs->name}}" /></a>
        </div>
          </div>		
        
      <div class="entry-details">
        <div class="entry-body">          
         
          <div class="entry-title">
            <span class="hexagon">
              <span class="corner1"></span>
              <span class="corner2"></span>
              <i class="fa fa-pencil"></i>
            </span>
            <h4> <a href="{{$blogs->name}}"> {{$blogs->title}} </a></h4>
            <div class="article-content">
              <p>{{$blogs->description}}</p>

<!-- <blockquote class="type5 "><q>“It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem some text are in so Ipsum. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form of this”</q></blockquote> -->

            </div>

          </div>
        </div>
        <!--entry-body ends-->
        <div class="entry-metadata">
          <h6 class="date"> <span class="hexagon2">
            <span class="corner1"></span>
            <span class="corner2"></span> <i class="fa fa-calendar"></i></span>{{$blogs->created_at}}</h6>
          <h6 class="author"> <span class="hexagon2">
            <span class="corner1"></span>
            <span class="corner2"></span> <i class="fa fa-user"></i></span> <a href="#" title="ram"> Admin </a> </h6>
  
        </div>
        <!--entry-metadata ends-->
        <div class="dt-sc-margin15"></div>
      </div>
      <!--entry-details ends-->
    </article>
    <!-- Begin comments -->
    
    <div id="comments" class="commententries">
      
    </div>
    <div id="respond" class="comment-respond">
      <h3>Enroll Your Words</h3>                          
      <form method="post" action="/blogs/news/18477647-alone-in-the-backyard/comments#comment_form" id="comment_form" accept-charset="UTF-8" class="comment-form"><input type="hidden" name="form_type" value="new_comment" /><input type="hidden" name="utf8" value="✓" />
      

      
      <div id="commentform">

        <p class="column dt-sc-one-half first">
          <input type="text" id="comment_author" name="comment[author]"  placeholder="Name" required="" />
          <input type="email" id="comment_email" name="comment[email]" placeholder="Email ID" required="" />
          <input type="text" required="" placeholder="Website (required)" name="website" id="website">
        </p>
        <p class="column dt-sc-one-half">
          <textarea id="comment_body" name="comment[body]" placeholder="Message"></textarea>
        </p>
        <p>
          <input type="submit" value="Add comment" class="btn" id="comment-submit" />
        </p>
      </div>
      <div class="dt-sc-margin50"></div>
       <div class="margin5"></div>
      
      </form>
    </div>
    
    <!-- End comments -->

  </div>
  <!-- Begin sidebar -->
  <div id="secondary">
  
  <aside class="widget">
    
    <h4 class="widgettitle">  Recent Posts  </h4>
    
    <div class="clear"></div>
    <ul class="recent-posts-widget">
      
      <li> 
        
        <a class="article__featured-image" href="/blogs/news/18477647-alone-in-the-backyard">
          <img src="//cdn.shopify.com/s/files/1/0687/0339/articles/blog5_1024x1024_1_medium.jpg?v=1475494657" alt="{{$blogs->i_image}}" />
        </a>
        
        <div class="entry-details">
          <div class="entry-title"><h5> <a href="/blogs/news/18477647-alone-in-the-backyard">{{$blogs->i_image}}</a> </h5></div>
          <div class="entry-metadata">
            <h6 class="date"> <span class="hexagon2">
              <span class="corner1"></span>
              <span class="corner2"></span> <i class="fa fa-calendar"></i></span> 24 Dec 2014 </h6>
            <h6><span class="hexagon2">
              <span class="corner1"></span>
              <span class="corner2"></span><i class="fa fa-comments"></i></span>  
              <a href="/blogs/news/18477647-alone-in-the-backyard">0</a> 
            </h6>
          </div>
          <!--entry-metadata ends-->
        </div>                        
      </li>
      
      <li> 
        
        <a class="article__featured-image" href="/blogs/news/18477531-monsoon-photography">
          <img src="//cdn.shopify.com/s/files/1/0687/0339/articles/blog4_9b9ad42b-ce27-41ce-bf6c-26c027187c3b_1024x1024_1_medium.jpg?v=1475494625" alt="MONSOON PHOTOGRAPHY" />
        </a>
        
        <div class="entry-details">
          <div class="entry-title"><h5> <a href="/blogs/news/18477531-monsoon-photography">MONSOON PHOTOGRAPHY</a> </h5></div>
          <div class="entry-metadata">
            <h6 class="date"> <span class="hexagon2">
              <span class="corner1"></span>
              <span class="corner2"></span> <i class="fa fa-calendar"></i></span> 24 Dec 2014 </h6>
            <h6><span class="hexagon2">
              <span class="corner1"></span>
              <span class="corner2"></span><i class="fa fa-comments"></i></span>  
              <a href="/blogs/news/18477531-monsoon-photography">0</a> 
            </h6>
          </div>
          <!--entry-metadata ends-->
        </div>                        
      </li>
      
      <li> 
        
        <a class="article__featured-image" href="/blogs/news/18143843-youtube-contrary-to-popular">
          <img src="//cdn.shopify.com/s/files/1/0687/0339/articles/blog6_1024x1024_1_medium.jpg?v=1475494583" alt="YOUTUBE – CONTRARY TO POPULAR" />
        </a>
        
        <div class="entry-details">
          <div class="entry-title"><h5> <a href="/blogs/news/18143843-youtube-contrary-to-popular">YOUTUBE – CONTRARY TO POPULAR</a> </h5></div>
          <div class="entry-metadata">
            <h6 class="date"> <span class="hexagon2">
              <span class="corner1"></span>
              <span class="corner2"></span> <i class="fa fa-calendar"></i></span> 18 Nov 2014 </h6>
            <h6><span class="hexagon2">
              <span class="corner1"></span>
              <span class="corner2"></span><i class="fa fa-comments"></i></span>  
              <a href="/blogs/news/18143843-youtube-contrary-to-popular">0</a> 
            </h6>
          </div>
          <!--entry-metadata ends-->
        </div>                        
      </li>
      
      <li> 
        
        <a class="article__featured-image" href="/blogs/news/18143839-vimeo-nam-malesuada-augue">
          <img src="//cdn.shopify.com/s/files/1/0687/0339/articles/blog_1024x1024_1_medium.jpg?v=1475494993" alt="VIMEO – NAM MALESUADA AUGUE" />
        </a>
        
        <div class="entry-details">
          <div class="entry-title"><h5> <a href="/blogs/news/18143839-vimeo-nam-malesuada-augue">VIMEO – NAM MALESUADA AUGUE</a> </h5></div>
          <div class="entry-metadata">
            <h6 class="date"> <span class="hexagon2">
              <span class="corner1"></span>
              <span class="corner2"></span> <i class="fa fa-calendar"></i></span> 18 Nov 2014 </h6>
            <h6><span class="hexagon2">
              <span class="corner1"></span>
              <span class="corner2"></span><i class="fa fa-comments"></i></span>  
              <a href="/blogs/news/18143839-vimeo-nam-malesuada-augue">0</a> 
            </h6>
          </div>
          <!--entry-metadata ends-->
        </div>                        
      </li>
      
    </ul>
  </aside>
  
  
  <aside class="widget widget_tag_cloud">
    
    
    <h4 class="widgettitle"> Tags</h4>
    
    <div class="tagcloud">
      <a href="https://citrus-demo.myshopify.com/blogs/news"  class="active">All</a>
      
      
      <a href="/blogs/news/tagged/audio" title="Show articles tagged audio">audio</a>
      
      
      
      <a href="/blogs/news/tagged/blog" title="Show articles tagged blog">blog</a>
      
      
      
      <a href="/blogs/news/tagged/chat" title="Show articles tagged chat">chat</a>
      
      
      
      <a href="/blogs/news/tagged/creative" title="Show articles tagged creative">creative</a>
      
      
      
      <a href="/blogs/news/tagged/design" title="Show articles tagged design">design</a>
      
      
      
      <a href="/blogs/news/tagged/news" title="Show articles tagged news">news</a>
      
      
      
      <a href="/blogs/news/tagged/video" title="Show articles tagged video">video</a>
      
      
    </div>
    
  </aside>
  
  
  <aside class="widget tweetbox" id="my_twitter-3">
    <h4 class="widgettitle"> Latest Tweets</h4>
    
    
    <div id="tweets_container">
      <a class="twitter-timeline" href="https://twitter.com/iamdesigning" data-widget-id="425498722839957505" data-border-color="#fff" data-tweet-limit="3" data-chrome="nofooter noheader">Tweets by @iamdesigning</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    </div>
    
    
  </aside>
  
  
  <aside class="widget">
    
    <h4 class="widgettitle">  TEXT WIDGET  </h4>
    
    
    <p> Citrus has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>
    
  </aside>  
  
</div>
<!-- secondary ends -->
<div class="dt-sc-margin20"></div>
  <!-- End sidebar -->
</div>

    </div>
    <!--main-content ends-->
@endsection