<div>
<b>from</b>:admin@ecommerce.com[mailto:azizdulal.ad@gmail.com]<br>
<b>Sent</b>:{{ $created_at }}<br>
<b>To</b>:{{ $name }}<br>
<b>Subject</b>:Order Confirmation<br>
<br>
    Hi, Thank You For the order.The order for {{ $p_id }} is confirmed and will be processed shortly.<br>

    <b>Order Id</b>:{{ $sn }} <br>
    <br>
    product detail: {{ $p_id }} <br>
    <b>product Name</b>:{{ $name }} <br>
    <br>
    <b>Location:</b>:{{ $landmark }},{{ $location }} <br>
    <b>Phone No:</b>:+977-{{ $phone }}s <br>
    <b>Product Quantity</b>:{{ $quantity }}<br>
    <b>Total</b>:Rs.{{ $price }}<br>
    <b>Message</b>: {{ $information }}
</div>