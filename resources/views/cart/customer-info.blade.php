@include('layouts.header')

<div class="content" data-content>
  <div class="wrap">
    <div class="main" role="main">
      <div class="main__header">
        <a class="logo logo--left" href="/"><img src="/images/logo.png?0" alt="Momos" /></a>  
        <h1 class="visually-hidden">
          Customer information
        </h1>


        <ul class="breadcrumb ">
          <li class="breadcrumb__item breadcrumb__item--completed">
            <a class="breadcrumb__link" data-trekkie-id="breadcrumb_cart_link" href="/products">Shop</a>
          </li>

          <li class="breadcrumb__item breadcrumb__item--current">
            <span class="breadcrumb__text" aria-current="step">Customer information</span>
            <svg class="icon-svg icon-svg--color-adaptive-light icon-svg--size-10 breadcrumb__chevron-icon" aria-hidden="true" focusable="false"> <use xlink:href="#chevron-right" /> </svg>
          </li>
        </ul>

      </div>
      <div class="main__content"> 
      <div class="step" data-step="contact_information">
        @if ($errors->any())
        <div class="form-group is-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <form class="edit_checkout" action="/sendemail/send/{{$product->id}}"  method="POST">
          {{csrf_field()}}
          <div class="step__sections">

            <div class="section section--shipping-address" data-shipping-address
            data-update-order-summary>
            <div class="section__content">
              <div class="fieldset" data-address-fields>
                <h2 class="section__title layout-flex__item layout-flex__item--stretch" id="main-header" tabindex="-1">
                  Contact information
                </h2>
                <div data-address-field="Name" data-autocomplete-field-container="true" class="field field--optional">
                  <label class="field__label" for="name">Name</label>
                  <div class="field__input-wrapper">
                    <input required="" placeholder="Name" class="field__input" size="20" type="text"  name="name" id="name" />
                  </div>
                </div>
                <div class="section__content" data-section="customer-information" data-shopify-pay-validate-on-load="false">
                  <div class="fieldset">
                    <div  class="field field--required">
                      <label class="field__label" for="email">Email</label>
                      <div class="field__input-wrapper">
                        <input required="" placeholder="Email" aria-required="true" class="field__input" size="20" type="email"  name="email" id="email" />
                      </div>
                    </div>        </div> 
                  </div> 
                  <div data-address-field="address2" data-autocomplete-field-container="true" class="field field--optional">
                    <label class="field__label" for="location">Location</label>
                    <div class="field__input-wrapper">
                      <input required="" placeholder="Location" class="field__input" size="20" type="text" name="location" id="location" />
                    </div>
                  </div>
                  <div class="field field--required">
                    <label class="field__label" for="landmark">Landmark</label>
                    <div class="field__input-wrapper">
                      <input required="" placeholder="Landmark" class="field__input" aria-required="true" size="20" type="text"  name="landmark" id="landmark" />
                    </div>
                  </div>
                  <div class="field field--optional">
                    <label class="field__label" for="phone">Phone </label>
                    <div class="field__input-wrapper">
                      <input required="" placeholder="Phone" class="field__input field__input--numeric" size="10" type="tel" name="phone" id="phone" />
                    </div>
                  </div>
                  <div class="field field--optional">
                    <label class="field__label" for="message">Request Message </label>
                    <div class="field__input-wrapper">
                      <textarea required="" placeholder="Request Message" class="field__input field__input--numeric" size="30" type="text" name="message"></textarea>
                    </div>
                  </div>
                  <input type="hidden" name="price" id="totalPriceSent" />
                  <input type="hidden" name="single_price" id="singlePriceSent" />
                  <input type="hidden" name="p_id" value="{{$product->cat_id}}" />
                  <input type="hidden" name="quantity" value="{{$quantity}}" />
                  <input type="hidden" name="product_name" value="{{$product->name}}">
                  <input type="hidden" id="totalDeliveryPrice" name="deliveryprice">
                </div> 
              </div> 
            </div> 

          </div>
          <div class="step__footer" data-step-footer>

            <button name="button" type="submit" onclick="deleteMe(event)" class="step__footer__continue-btn btn "  aria-busy="false">
              <span class="btn__content">
                Order Now
              </span>
              <svg class="icon-svg icon-svg--size-18 btn__spinner icon-svg--spinner-button" aria-hidden="true" focusable="false"></svg>
            </button>

            <a class="step__footer__previous-link" href="/products"><svg focusable="false" aria-hidden="true" class="icon-svg icon-svg--color-accent icon-svg--size-10 previous-link__icon" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10"><path d="M8 1L7 0 3 4 2 5l1 1 4 4 1-1-4-4"/></svg><span class="step__footer__previous-link-content">Return to shop</span></a>
          </div>
        </form>
      </div>
    </div>

    <script type="text/javascript"> 
      function deleteMe(evt){
        alert('Successfully Ordered')
      }
    </script>

    <div class="main__footer">
      <div role="contentinfo" aria-label="Footer">
        <p class="copyright-text">
          All rights reserved Chini Group
        </p>
      </div>


    </div>
  </div>
  <div class="sidebar" role="complementary">
    <div class="sidebar__header">
      <h1 class="visually-hidden">
        Customer information
      </h1>
    </div>
    <div class="sidebar__content">
      <div id="order-summary" class="order-summary order-summary--is-collapsed" data-order-summary>
        <h2 class="visually-hidden-if-js">Order summary</h2>

        <div class="order-summary__sections">
          <div class="order-summary__section order-summary__section--product-list">
            <div class="order-summary__section__content">
              <table class="product-table">
                <tbody data-order-summary-section="line-items">
                  <tr class="product" data-product-id="400260495" data-variant-id="951389807" data-product-type="T-Shirts" data-customer-ready-visible>
                    <td class="product__image">
                      <div class="product-thumbnail">
                        <div class="product-thumbnail__wrapper">
                          <img alt="{{$product->name}}" class="product-thumbnail__image" src="/uploads/{{$product->image1}}" />
                        </div>
                        <span id="quantities" class="product-thumbnail__quantity" aria-hidden="true">{{$quantity}}</span>
                      </div>

                    </td>
                    <td class="product__description">
                      <span class="product__description__name order-summary__emphasis">{{$product->name}}</span>
                      <span class="product__description__variant order-summary__small-text">NTPL Bazar</span>

                    </td>
                    <td class="product__quantity visually-hidden">
                      1
                    </td>
                    <td  class="product__price">
                      Rs.<span id="singleprice" class="order-summary__emphasis">{{$product->sale_price}}</span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>



          <div class="order-summary__section order-summary__section--total-lines" data-order-summary-section="payment-lines">
            <table class="total-line-table">
              <caption class="visually-hidden">Cost summary</caption>
              <thead>
                <tr>
                  <th scope="col"><span class="visually-hidden">Description</span></th>
                  <th scope="col"><span class="visually-hidden">Price</span></th>
                </tr>
              </thead>
              <tbody class="total-line-table__tbody">
                <tr class="total-line total-line--subtotal">
                  <th class="total-line__name" scope="row">Product Total</th>
                  <td class="total-line__price">
                    Rs.<span class="order-summary__emphasis" id="totalPrice" data-checkout-subtotal-price-target="12000">
                    </span>
                  </td>
                </tr>


                <tr class="total-line total-line--shipping">
                  <th class="total-line__name" scope="row">
                    Delivery
                  </th>
                  <td class="total-line__price">
                    Rs.<span class="order-summary__emphasis" id="deliveryPrice" data-checkout-total-shipping-target="1800">100.00</span>
                  </td>
                </tr>


                <tr class="total-line total-line--taxes hidden" data-checkout-taxes>
                  <th class="total-line__name" scope="row">Taxes</th>
                  <td class="total-line__price">
                    Rs.<span class="order-summary__emphasis" data-checkout-total-taxes-target="0">0</span>
                  </td>
                </tr>




              </tbody>
              <tfoot class="total-line-table__footer">
                <tr class="total-line">
                  <th class="total-line__name payment-due-label" scope="row">
                    <span class="payment-due-label__total">Total</span>
                  </th>
                  <td class="total-line__price payment-due">
                    <span class="payment-due__currency">NPR</span>
                    Rs.<span class="payment-due__price" id="netPrice" data-checkout-payment-due-target="13800">                       
                    </span>
                  </td>
                </tr>
              </tfoot>
            </table>

            <div class="visually-hidden" aria-live="polite" aria-atomic="true" role="status">
              Updated total price:
              <span data-checkout-payment-due-target="13800" >

              </span>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script>

  $(document).ready(function(){
    $qty = $("#quantities").html();
    $price = $("#singleprice").html();
    $total_price = $price * $qty;
    $("#totalPrice").html( $total_price);
    $netPrice = $total_price + parseInt($("#deliveryPrice").html(),10);
    $("#netPrice").html($netPrice);
    $("#totalPriceSent").val($netPrice);
    $("#singlePriceSent").val($price);
    $("#totalDeliveryPrice").val(parseInt($("#deliveryPrice").html(),10));
  });

</script>