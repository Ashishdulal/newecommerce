@include('layouts.header')

<!--wrapper starts-->
<div class="wrapper">
  <!--top-content starts-->
  <div class="top-content home" id="section_1">


    <div id="shopify-section-slider" class="shopify-section">
      <div data-section-id="slider" data-section-type="slider-section">

        <!-- Begin slider -->
        <!-- **Slider Section** -->
        <div class="tp-banner-container">
          <div class="tp-banner" >
            <ul>  <!-- SLIDE  -->


        @foreach($sliders as $slider)
              <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" >
                <!-- MAIN IMAGE -->
                
                <img src="/uploads/{{$slider->image}}"  alt="banner4"  data-bgposition="center top" data-kenburns="on" data-duration="9000" data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="110" data-bgpositionend="center center">
                
                <!-- LAYERS -->
                
                <!-- LAYER NR. 1 -->
                <div class="tp-caption firstone white_big_border tp-fade tp-resizeme"
                data-x="25"
                data-y="312" 
                data-speed="300"
                data-start="1500"
                data-easing="Power3.easeInOut"
                data-splitin="none"
                data-splitout="none"
                data-elementdelay="0.1"
                data-endelementdelay="0.1"
                data-endspeed="300"
                style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">{{$slider->title}}
              </div>
              
              
              <!-- LAYER NR. 2 -->
              <div class="tp-caption white_small_shadow tp-fade tp-resizeme"
              data-x="325"
              data-y="476" 
              data-speed="300"
              data-start="3000"
              data-easing="Power3.easeInOut"
              data-splitin="none"
              data-splitout="none"
              data-elementdelay="0.1"
              data-endelementdelay="0.1"
              data-endspeed="300"
              style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">{{$slider->description}}
            </div>
        </li>
        @endforeach      

</ul>
</div>
</div><!-- **Slider Section - Ends** -->
<!-- End slider -->

</div>


</div>

</div>
<!--top-content ends-->

<!--main-content starts-->
<div id="main-content">

  <!-- BEGIN content_for_index -->
  <div id="shopify-section-1504159376790" class="shopify-section">
    <div data-section-id="1504159376790" data-section-type="about-page">

      <!--about starts-->
      <section class="about" id="section_2">
        <div class="">
          <section class="shop" id="section_5">
            <div class="dt-sc-margin50"></div> 
            <!--container starts-->
            <div class="container">

              <!--main-title starts-->
              <div class="main-title">
                <h2 class="align-center"> 
                  featured
                  <span class="wlast">products</span>
                  <span class="small-line"></span></h2>
                </div>
                
                <div class="content-full-width" id="primary">
                  <div class="portfolio-carousel-wrapper gallery">
                    <div class="dt-sc-margin30"></div>
                    <!-- **Portfolio Carousel** -->
                    <ul class="portfolio-carousel products">
                      @foreach($products as $product)
                      <li class="product-four-column product-wrapper">
                        <!-- Prodcut Wrapper -->
                        <div class="">
                          <div class="product-container">
                            <a href="/product-detail/{{$product->id}}">
                              <div class="product-thumb">
                                <img src="/uploads/{{$product->image1}}" alt="Flying Ninja" />
                                
                                
                                <span class="onsale"><span>Sale!</span></span>
                                
                                
                              </div>
                              <!-- Product Thumbnail -->
                              <div class="product-title">
                                <h3>{{$product->name}}</h3>
                              </div>
                            </a>
                            <div class="product-details">

                              <span class="price"> <ins><del>Rs.{{$product->price}}</del>&nbsp;<span class="amount">  Rs.{{$product->sale_price}} </span></ins></span>
                              
                              <form action="" method="post" class="variants clearfix">
                                <div class="product-options ">
                                  <div class=" multiple">
                                    <input type="submit"  class="btn addtocart add_to_cart_button dt-sc-button button  product_type_simple " name="add" value="" />
                                  </div>
                                </div>
                              </form>
                            </div>
                            <div class="product-details-hover">
                              <h3><a href="/product-detail/{{$product->id}}">{{$product->name}}</a></h3>
                              
                              <span class="price"><del>Rs.{{$product->price}}</del>&nbsp;<span class="amount">Rs.{{$product->sale_price}} </span></span>
                              
                              <div class="cus-product-buttons">
                                <form  action="/cart/add" method="post" class="variants clearfix">
                                  <!-- Begin product options -->
                                  <div class="product-options">
                                    <div class="select clearfix" style="display:none">
                                    </div>
                                    <div class="purchase-section">

                                      <a href="/product-detail/{{$product->id}}" class="view_details" title="View Details"></a>
                                      
                                    </div>
                                  </div>
                                  <!-- End product options -->
                                </form>
                                <div class="add-wishlist">
                                  <div class="add-wishlist-button show">

                                    <a class="add_to_cart_button btnwish" data-product-type="simple" data-product-id="56" href="/product-detail/{{$product->id}}" title="Buy Product"></a>

                                    
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- Prodcut Wrapper Ends -->
                      </li>
                      @endforeach

                    </ul>
                    <div class="carousel-arrows">
                      <a href="#" title="" class="portfolio-prev-arrow"> <span class="fa fa-caret-left"> </span> </a>
                      <a href="#" title="" class="portfolio-next-arrow"> <span class="fa fa-caret-right"> </span> </a>
                    </div>

                  </div>
                  <a class="dt-sc-button medium" href="/products">All Products</a>
                </div>
              </div>
              <!--container starts-->
            </section>
            <!-- shop ends-->

          </div>
        </div>
        <div id="shopify-section-1504163934491" class="shopify-section index-section">
          <div data-section-id="1504163934491" data-section-type="team-section">
          </div>
          <div class="dt-sc-margin50" id="menu-header-menu"></div>
          <!--container starts-->
          <div class="container dt-sc-hr-invisible" id="infoabout">

            <!--main-title starts-->
            <div class="main-title">
              <h2 class="align-center">
                About
                <span class="wlast">NepCommerce</span>
                <span class="small-line"></span></h2>
              </div>
              <!--main-title ends-->
              <div class="dt-sc-hr-invisible"></div>


              <!--column one-half starts-->
              <div class="column dt-sc-one-half">
                <!--our-team starts-->   
                <div class="dt-sc-team">
                  <div class="dt-sc-hr-invisible-small"></div>
                  <!--founder-thumb starts-->
                  <figure class="founder-thumb">
                    <span class="hexagon">
                      <span class="corner1"></span>
                      <span class="corner2"></span>
                    </span>
                    <span class="hexagon2">
                      <span class="corner1"></span>
                      <span class="corner2"></span>
                    </span>
                    <div class="hexagon-image">
                      <div class="hexagon-in1">
                        <div class="hexagon-in2 founder-image"></div></div>
                      </div>
                    </figure>  
                    <!--founder-thumb ends-->
                    <div class="dt-sc-hr-invisible-small"></div>
                    <!-- start content page -->






                    <h5>Skyler White</h5>


                    <h6>Ceo & Founder</h6>


                    <p>Saleem naijar kaasram eerie can be disbursed in the wofl like of a fox that is her thing smaoasa <br>lase lemedds laasd pamade eleifend sapien.</p>



                    <ul class="dt-sc-social-icons">

                      <li><a href="#"><span class="hexagon2">
                        <span class="corner1"></span>
                        <span class="corner2"></span>                                    
                        <i class="fa fa-facebook"></i></span></a></li>


                        <li><a href="#"><span class="hexagon2">
                          <span class="corner1"></span>
                          <span class="corner2"></span>
                          <i class="fa fa-twitter"></i> </span></a></li>


                          <li><a href="#"><span class="hexagon2">
                            <span class="corner1"></span>
                            <span class="corner2"></span>
                            <i class="fa fa-linkedin"></i> </span></a></li>





                            <li><a href="#"><span class="hexagon2">
                              <span class="corner1"></span>
                              <span class="corner2"></span>
                              <i class="fa fa-youtube"></i> </span></a></li>


                              <li><a href="#"><span class="hexagon2">
                                <span class="corner1"></span>
                                <span class="corner2"></span>
                                <i class="fa fa-google-plus"></i> </span></a></li>

                              </ul>

                              <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">More Info</button> -->

                            </div>

                            <!-- Modal -->
<!-- <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div> -->


<!--team ends-->                  
</div>
<!--column one-half ends-->

<!--column one-half starts -->
<div class="column dt-sc-one-half last">
  <ul class="slider">

    <li><img src="//cdn.shopify.com/s/files/1/0687/0339/files/about_slider_img_1_5340ef75-3204-4411-a4de-e08d027f9610.jpg?v=1504160161" alt="slider" /></li>
    
    
    <li><img src="//cdn.shopify.com/s/files/1/0687/0339/files/about_slider_img_2_54f390e7-bd3b-4bb6-8a26-e72bf01fcc1e.jpg?v=1504160162" alt="slider" /></li>
    
    
    <li><img src="//cdn.shopify.com/s/files/1/0687/0339/files/about_slider_img_3_5e83331b-a6de-4f5b-a882-f46c85659759.jpg?v=1504160205" alt="slider" /></li>
    
  </ul>
  <div class="shadow"></div>
</div>
<!--column one-half ends-->      


</div>
<!--container ends-->
<div class="dt-sc-margin20"></div>
<div class="dt-sc-margin30"></div>
</section>
<!--about ends-->

</div>





<style>


  .founder-image{background-image:url(/images/founder-img_35cf5b85-37fe-4554-a6df-6045d01bdc85.jpg?v=1504159648);}

  .about .bx-wrapper{ background:url(//cdn.shopify.com/s/files/1/0687/0339/files/slider-shadow_e22aa70f-d5db-4247-9744-2c0239304607.png?v=1504160338) no-repeat left 92%;}


</style>












</div><div id="shopify-section-1504160567789" class="shopify-section">
  <div data-section-id="1504160567789" data-section-type="icon-section">

    <!--full-width-section starts-->
    <div class="about-us-detail">
      <!--container starts-->
      <div class="container">

        <h3>WHAT WE DO?</h3>
        
        
        <h6>We are the award winning website designers from india</h6>
        <div class="dt-sc-margin30"></div>
        
        
        <!--column one-fourth starts-->
        <div class="column dt-sc-one-fourth">
          <!--icon-content starts-->
          <div class="dt-sc-icon-content type1">
            <!--hexagon-shapes starts-->
            
            <div class="hexagon-shapes">
              <span class="hexagon">
                <span class="corner1"></span>
                <span class="corner2"></span>
              </span>
              <span class="hexagon2">
                <span class="corner1"></span>
                <span class="corner2"></span>
                <i class="fa fa-headphones"></i>
              </span>
            </div>
            
            <!--hexagon-shapes ends-->
            
            <h4>INNOVATIVE</h4>
            
            
            <p>Pellentesquemattis arcu malesuada in. Don ecurna sem, rutrum sit amet pellentesque vel suscipit at metus...</p>
            
          </div>
          <!--icon-content ends-->
        </div>
        <!--column one-fourth ends-->
        
        
        <!--column one-fourth starts-->
        <div class="column dt-sc-one-fourth">
          <!--icon-content starts-->
          <div class="dt-sc-icon-content type1">
            <!--hexagon-shapes starts-->
            
            <div class="hexagon-shapes">
              <span class="hexagon">
                <span class="corner1"></span>
                <span class="corner2"></span>
              </span>
              <span class="hexagon2">
                <span class="corner1"></span>
                <span class="corner2"></span>
                <i class="fa fa-lightbulb-o"></i>
              </span>
            </div>
            
            <!--hexagon-shapes ends-->
            
            <h4>CREATIVITY</h4>
            
            
            <p>Consetetur sadipscing elitr, sed diamnon ue eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam..</p>
            
          </div>
          <!--icon-content ends-->
        </div>
        <!--column one-fourth ends-->
        
        
        <!--column one-fourth starts-->
        <div class="column dt-sc-one-fourth">
          <!--icon-content starts-->
          <div class="dt-sc-icon-content type1">
            <!--hexagon-shapes starts-->
            
            <div class="hexagon-shapes">
              <span class="hexagon">
                <span class="corner1"></span>
                <span class="corner2"></span>
              </span>
              <span class="hexagon2">
                <span class="corner1"></span>
                <span class="corner2"></span>
                <i class="fa fa-bar-chart-o"></i>
              </span>
            </div>
            
            <!--hexagon-shapes ends-->
            
            <h4>USER EXPERIENCE</h4>
            
            
            <p>At vero eos et accusamarcu malesuada in. ecurna sem, rutrum sit amet pellentesque vel suscipit at metus...</p>
            
          </div>
          <!--icon-content ends-->
        </div>
        <!--column one-fourth ends-->
        
        
        <!--column one-fourth starts-->
        <div class="column dt-sc-one-fourth last">
          <!--icon-content starts-->
          <div class="dt-sc-icon-content type1">
            <!--hexagon-shapes starts-->
            
            <div class="hexagon-shapes">
              <span class="hexagon">
                <span class="corner1"></span>
                <span class="corner2"></span> 
              </span>
              <span class="hexagon2">
                <span class="corner1"></span>
                <span class="corner2"></span>
                <i class="fa fa-plane"></i>
              </span>
            </div>
            
            <!--hexagon-shapes ends-->
            
            <h4>EXCLUSIVE SUPPORT</h4>
            
            
            <p>Orelentesquemattis arcu malesuada in. Don ecurna sem, rutrum sit amet pellentesque vel suscipit at metus...</p>
            
          </div>
          <!--icon-content ends-->
        </div>
        <!--column one-fourth ends-->
        
      </div>
      <!--container ends-->
    </div>
    <!--full-width-section ends-->
    
  </div>










  <style>



    .about-us-detail { background:url(//cdn.shopify.com/s/files/1/0687/0339/files/full-width-bg-about_fe763d1e-76b8-4833-8bd8-f2b5d2c1309f.jpg?v=1504160867) repeat left top; }

    

    .dt-sc-icon-content.type1 .hexagon2 .fa{color:#000000;}

    .dt-sc-icon-content h4{color:#003993;}

    .dt-sc-icon-content p{color:#000000;}

  </style>












</div><div id="shopify-section-1504161297864" class="shopify-section index-section">
  <div data-section-id="1504161297864" data-section-type="services-section">
    <!--services-->
    <!--services starts-->      
    
    <section class="services" id="section_3">
      <div class="dt-sc-hr-invisible-small"></div>
      <div class="dt-sc-margin20"></div>
      <!--container starts-->
      <div class="container">

        <!--main-title starts-->
        <div class="main-title">
          <h2 class="align-center">Our
            <span class="wlast">Services</span>
            <span class="small-line"></span></h2>
          </div>
          <!--main-title ends-->
          
          
          <p class="align-center grey">Our Passion leads to design, design leads to performance, performance leads to success. We believe that apps and websites should not only be eye<br>catching but actually provide a great user experience that users will remember.</p>
          
          
          <p class="align-center grey margin5"></p>
          <div class="dt-sc-margin20"></div>
          <!--theme-service starts-->
          <article class="theme-service">

            <!--column one-half starts-->
            <?php $count=1; ?>
        @foreach($Services as $service)
        @if($count%2  == 0 )
        <!--column one-half starts-->
            <div class="column dt-sc-one-half right last">
              <!--hexagon-shapes starts-->
              
              <div class="column dt-sc-one-sixth">
                <img style="max-width: 100%;" src="/uploads/{{$service->image}}">
                <!-- <span class="hexagon">
                  <span class="corner1"></span>
                  <span class="corner2"></span>
                </span>
                <span class="hexagon2">
                  <span class="corner1"></span>
                  <span class="corner2"></span>
                </span> -->                            
              </div>    
              
              <!--hexagon-shapes ends-->
              <!--five-sixth starts-->
              <div class="column no-space dt-sc-five-sixth">

                <h5>{{$service->name}}</h5>
                
                
                <p><?php echo ($service->description)?></p>
                
              </div>
              <!--five-sixtht ends-->
            </div>
            
            <!--column one-half left ends-->
            @else()
                         <div class="column dt-sc-one-half left">
              <!--five-sixth starts-->
              <div class="column no-space dt-sc-five-sixth">

                <h5>{{$service->name}}</h5>
                
                
                <p><?php echo ($service->description)?></p>
              </div>
              <!--five-sixth ends-->
              <!--hexagon-shapes starts-->
              
              <div class="column dt-sc-one-sixth image_service">
                <img style="max-width: 100%;" src="/uploads/{{$service->image}}">
                <!-- <span class="hexagon">
                  <span class="corner1"></span>
                  <span class="corner2"></span>
                </span>
                <span class="hexagon2">
                  <span class="corner1"></span>
                  <span class="corner2"></span>
                  <i class="fa fa-stethoscope"></i>
                </span>           -->                  
              </div>   
              
              <!--hexagon-shapes ends-->
            </div>
            @endif
        <?php $count++;?>
        @endforeach
            <!--column one-half left ends-->
            
            
           
          </article>
          <!--theme-service ends-->
          </div>
        <!--container ends-->
        <div class="dt-sc-margin50"></div>
        <div class="dt-sc-margin10"></div>
      </section>
      <!--services ends-->
    </div>





    <style>

      .theme-service h5{color:#181818;}

      .theme-service .hexagon-shapes .fa{color:#181818;}

      .theme-service p{color:#7c7c7c;}

    </style>

  </div>
  <div id="shopify-section-1504162259889" class="shopify-section index-section">
    <div data-section-id="1504162259889" data-section-type="number-section">

      <!--full-width-section starts-->
      <div class="parallax statistic" >
        <!--container starts-->
        <div class="container">
          <!--main-title starts-->
          
          <div class="main-title">
            <h3 class="align-center"> 


              SOME
              
              
              
              FUN
              
              
              
              FACTS
              
              
              
              ABOUT
              
              
              
              
              <span class="wlast">NepCommerce </span>
              <span class="small-line"></span></h3>
            </div>
            
            <!--main-title ends-->
            
            <p>Each completed project makes us even more hungry, hungry for more designs, more code and at least some more twinkies.<br>As a result we deliver a better web experience.</p>
            <div class="dt-sc-hr-invisible"></div>
            
            
            
            <!--column one-fourth starts-->
            <div class="column dt-sc-one-fourth">
              <div class="dt-sc-icon-content type2">
                <!--hexagon-shapes starts-->
                <div class="hexagon-shapes">
                  <span class="hexagon">
                    <span class="corner1"></span>
                    <span class="corner2"></span>
                  </span>
                  <span class="hexagon2">
                    <span class="corner1"></span>
                    <span class="corner2"></span>
                  </span>
                  <div class="hexagon-icon">

                    <i class="fa fa-users"></i>
                    
                    
                    <h2 data-value="4555" class="dt-sc-num-count">4555</h2>
                    
                  </div>
                </div>
                <!--hexagon-shapes ends-->
                
                <h5>Marketplace Items</h5>
                
              </div>
            </div>
            <!--column one-fourth ends-->
            
            <!--column one-fourth starts-->
            <div class="column dt-sc-one-fourth">
              <div class="dt-sc-icon-content type2">
                <!--hexagon-shapes starts-->
                <div class="hexagon-shapes">
                  <span class="hexagon">
                    <span class="corner1"></span>
                    <span class="corner2"></span>
                  </span>
                  <span class="hexagon2">
                    <span class="corner1"></span>
                    <span class="corner2"></span>
                  </span>
                  <div class="hexagon-icon">

                    <i class="fa fa-book"></i>
                    
                    
                    <h2 data-value="15" class="dt-sc-num-count">15</h2>
                    
                  </div>
                </div>
                <!--hexagon-shapes ends-->
                
                <h5>Talented Employees</h5>
                
              </div>
            </div>
            <!--column one-fourth ends-->
            
            <!--column one-fourth starts-->
            <div class="column dt-sc-one-fourth">
              <div class="dt-sc-icon-content type2">
                <!--hexagon-shapes starts-->
                <div class="hexagon-shapes">
                  <span class="hexagon">
                    <span class="corner1"></span>
                    <span class="corner2"></span>
                  </span>
                  <span class="hexagon2">
                    <span class="corner1"></span>
                    <span class="corner2"></span>
                  </span>
                  <div class="hexagon-icon">

                    <i class="fa fa-rocket"></i>
                    
                    
                    <h2 data-value="3853" class="dt-sc-num-count">3853</h2>
                    
                  </div>
                </div>
                <!--hexagon-shapes ends-->
                
                <h5>Items Sold</h5>
                
              </div>
            </div>
            <!--column one-fourth ends-->
            
            <!--column one-fourth starts-->
            <div class="column dt-sc-one-fourth">
              <div class="dt-sc-icon-content type2">
                <!--hexagon-shapes starts-->
                <div class="hexagon-shapes">
                  <span class="hexagon">
                    <span class="corner1"></span>
                    <span class="corner2"></span>
                  </span>
                  <span class="hexagon2">
                    <span class="corner1"></span>
                    <span class="corner2"></span>
                  </span>
                  <div class="hexagon-icon">

                    <i class="fa fa-twitter"></i>
                    
                    
                    <h2 data-value="287" class="dt-sc-num-count">287</h2>
                    
                  </div>
                </div>
                <!--hexagon-shapes ends-->
                
                <h5>Happy Clients</h5>
                
              </div>
            </div>
            <!--column one-fourth ends-->
            
            
          </div>
          <!--container ends-->
        </div>
        
        <!--full-width-section ends-->
      </div>




      <style>



        .statistic { background:url(//cdn.shopify.com/s/files/1/0687/0339/files/full-width-funfacts_992a8fe3-78ad-47db-8ea3-215c53856ebc.jpg?v=1504162530) repeat-y 50% 0 fixed; }

        

        .dt-sc-icon-content.type2 .hexagon-shapes .fa{color:#181818;}

        .dt-sc-icon-content.type2 h2{color:#181818;}

        .statistic h5{color:#ffffff;}
      </style>











    </div>
    <section class="team" id="section_6">
      <div class="dt-sc-margin50"></div> 
      <div class="dt-sc-margin10"></div> 
      <!--container starts-->
      <div class="container">

        <!--main-title starts-->
        <div class="main-title">
          <h2 class="align-center"> 
            OUR
            <span class="wlast">TEAM</span>
            <span class="small-line"></span></h2>
          </div>
          <!--main-title ends--> 
          
          
          <p class="align-center grey">Our Passion leads to design, design leads to performance, performance leads to success. We believe that apps and websites should not only be eye <br>catching but actually provide a great user experience that users will remember.</p>
          <div class="dt-sc-margin30"></div>
          
          <?php $counter=0; ?>
          <!--column one-fourth starts-->
          @foreach ($teammembers as $team)
          @if($counter <= 3)
          <div class="column dt-sc-one-fourth">
            <!--our-team starts-->
            <div class="dt-sc-team">
              <!--team-thumb starts-->
              <figure class="team-thumb">
                <span class="hexagon">
                  <span class="corner1"></span>
                  <span class="corner2"></span>
                </span>
                <span class="hexagon2">
                  <span class="corner1"></span>
                  <span class="corner2"></span>
                </span>
                <div class="hexagon-image">
                  <div class="hexagon-in1">
                    <div class="hexagon-in2" style="background: url(/uploads/{{$team->image}});     background-size: cover;" >
                    </div></div>
                  </div>
                </figure>  
                <!--team-thumb-thumb ends-->
                
                <h5>{{$team->name}}</h5>
                
                
                <h6>{{$team->designation}}</h6>
                
                
                <p><?php echo ($team->description)?></p>
                
                <ul class="dt-sc-social-icons">

                  <li><a href="{{$team->facebook_link}}" target="_blank"><span class="hexagon2">
                    <span class="corner1"></span>
                    <span class="corner2"></span>
                    <i class="fa fa-facebook"></i> </span></a></li>
                    
                    
                    <li><a href="{{$team->twitter_link}}" target="_blank"><span class="hexagon2">
                      <span class="corner1"></span>
                      <span class="corner2"></span>
                      <i class="fa fa-twitter"></i> </span></a></li>
                      
                      
                      <li><a href="{{$team->instagram_link}}" target="_blank"><span class="hexagon2">
                        <span class="corner1"></span>
                        <span class="corner2"></span>
                        <i class="fa fa-instagram"></i> </span></a></li>
                        
                        
                        <li><a href="{{$team->Youtube_link}}" target="_blank "><span class="hexagon2">
                          <span class="corner1"></span>
                          <span class="corner2"></span>
                          <i class="fa fa-youtube"></i> </span></a></li>
                        </ul>                        
                      </div>
                      <!--our-team ends-->
                    </div>
                    @endif
                    <?php $counter++;?>
                    @endforeach
                    <!--column one-fourth ends-->   
                    
                    
                    <div class="dt-sc-margin30"></div>
                    <div class="dt-sc-margin10"></div>
                    
                  </div>
                  <!--container ends-->
                </section>
                <div id="shopify-section-1504162990874" class="shopify-section index-section">
                  <div data-section-id="1504162990874" data-section-type="portfolio-section">

                  </div>

                </div><div id="shopify-section-1504163438755" class="shopify-section index-section">
                  <div data-section-id="1504163438755" data-section-type="progress-section">

                    <div class="parallax progress-bar">
                      <!--container starts-->
                      <div class="container"> 
                        <!--main-title starts-->
                        
                        <div class="main-title">
                          <h3 class="align-center">OUR LEVEL OF
                            <span class="wlast">brilliance</span>
                            <span class="small-line"></span></h3>
                          </div>
                          <div class="dt-sc-margin10"></div>
                          
                          <!--main-title ends-->
                          
                          <p class="size">Professional & Outstanding ideas of our passionate team makes us unique in every sense. Our Services. Our services are delivered by our team with years of experience are <br>passionate about developing business.</p>
                          <div class="dt-sc-margin30"></div>v
                          
                          
                          <!--column one-fourth starts-->
                          <div class="column dt-sc-one-fourth">
                            <!--donutchart-medium starts-->
                            <div class="dt-sc-donutchart-medium">
                              <!--donutchart starts-->
                              
                              <div class="dt-sc-donutchart1" data-percent="90" ></div>
                              
                              <!--donutchart ends-->
                              
                              <h6 class="dt-sc-donutchart-title orange">GRAPHIC DESIGN</h6>
                              
                            </div>
                            <!--donutchart-medium ends-->
                          </div>                        
                          <!--column one-fourth ends-->
                          
                          
                          <!--column one-fourth starts-->
                          <div class="column dt-sc-one-fourth">
                            <!--donutchart-medium starts-->
                            <div class="dt-sc-donutchart-medium">
                              <!--donutchart starts-->
                              
                              <div class="dt-sc-donutchart2" data-percent="80"></div>
                              
                              <!--donutchart ends-->
                              
                              <h6 class="dt-sc-donutchart-title blue">WEB DEVELOPMENT</h6>
                              
                            </div>
                            <!--donutchart-medium ends-->
                          </div>                        
                          <!--column one-fourth ends-->
                          
                          
                          <!--column one-fourth starts-->
                          <div class="column dt-sc-one-fourth">
                            <!--donutchart-medium starts-->
                            <div class="dt-sc-donutchart-medium">
                              <!--donutchart starts-->
                              
                              <div class="dt-sc-donutchart3" data-percent="66" ></div>
                              
                              <!--donutchart ends-->
                              
                              <h6 class="dt-sc-donutchart-title light-green"> WEB DESIGN</h6>
                              
                            </div>
                            <!--donutchart-medium ends-->
                          </div>                        
                          <!--column one-fourth ends-->
                          

                          
                          <!--column one-fourth starts-->
                          <div class="column dt-sc-one-fourth last">
                            <!--donutchart-medium starts-->
                            <div class="dt-sc-donutchart-medium">
                              <!--donutchart starts-->
                              
                              <div class="dt-sc-donutchart4" data-percent="60" ></div>
                              
                              <!--donutchart ends-->
                              
                              <h6 class="dt-sc-donutchart-title yellow">CUSTOMER SUPPORT</h6>
                              
                            </div>
                            <!--donutchart-medium ends-->
                          </div>                        
                          <!--column one-fourth ends-->
                          
                        </div>
                        <!--container ends-->
                      </div>
                      
                      <!--full-width-section ends-->
                    </div>




                    <style>




                      .progress-bar { background:url(//cdn.shopify.com/s/files/1/0687/0339/files/full-width-brilliance_0081c701-ded1-4b77-85ec-393c8252ac62.jpg?v=1504163506) repeat-y 50% 0 fixed; }

                      


                    </style>


                  </div>
                  <div id="shopify-section-1504163783583" class="shopify-section index-section"><div data-section-id="1504163783583" data-section-type="shop-section">

                    <!--shop starts-->
                    
                    
                    
                  </div>



                  <style>




                    .team-image2{background-image:url(//cdn.shopify.com/s/files/1/0687/0339/files/team_img_2_cc590a57-df9b-4324-98bf-bcc2161c5787.jpg?v=1504164947);}

                    .team-image3{background-image:url(//cdn.shopify.com/s/files/1/0687/0339/files/team_img_3_d861cdd7-4381-4471-848c-0a6cbf7b07bf.jpg?v=1504164960);}

                    .team-image4{background-image:url(//cdn.shopify.com/s/files/1/0687/0339/files/team_img_4_1_bba2b402-7892-4841-8474-d593ac5e5dba.jpg?v=1504164967);}
                  </style>

                </div><div id="shopify-section-1504165014542" class="shopify-section index-section"><div data-section-id="1504165014542" data-section-type="client-section">

                  <!--full-width-section starts-->
                  <div class="team">
                    <div class="dotted-full-width">
                      <!--container starts-->
                      <div class="container">
                        <div class="dt-sc-margin10"></div>
                        
                        <h4 class="medium-weight margin5">OUR VALUABLE CLIENTS</h4>
                        
                        
                        <p class="grey size">We are the award winning website designers from india</p>
                        <div class="dt-sc-hr-invisible-small"></div>
                        
                        <!--jcarousel-wrapper starts-->
                        <div class="jcarousel-wrapper">
                          <!--jcarousel starts-->
                          <div class="jcarousel">
                            <ul>


                              @foreach($valuableclient as $vclient)
                              <li><a href=""><img src="/uploads/{{$vclient->image}}" title="" alt="{{$vclient->name}}" /></a></li>
                              @endforeach
                              
                            </ul>
                          </div>
                          <!--jcarousel ends-->
                          <div class="dt-sc-margin20"></div>
                          <p class="jcarousel-pagination">
                            <a class="active" href="#1">1</a>
                            <a href="#2">2</a>
                            <a href="#3">3</a>
                            <a href="#4">4</a>
                            <a href="#5">5</a>
                            <a href="#6">6</a>
                          </p>
                        </div>
                        <!--jcarousel-wrapper ends-->
                      </div>
                      <!--container ends-->
                    </div>
                  </div>
                  
                </div>

                <style>
                  .team .dotted-full-width { background:url(//cdn.shopify.com/s/files/1/0687/0339/files/full_width_dotted_team_1.jpg?v=1504165096) repeat left top; }

                </style>

              </div><div id="shopify-section-1504165233825" class="shopify-section index-section">
                <div data-section-id="1504165233825" data-section-type="testimonial-section">
                  <!--full-width-section starts-->
                  
                  <div class="parallax full-width-quote">
                    <!--tweet-content starts-->
                    <div class="dt-sc-testimonial-wrapper type2 align-center">
                      <!--container starts-->
                      <div class="container">
                        <!--hexagon-figure starts-->
                        
                        <div class="hexagon-shapes">
                          <span class="hexagon">
                            <span class="corner1"></span>
                            <span class="corner2"></span>
                          </span>
                          <span class="hexagon2">
                            <span class="corner1"></span>
                            <span class="corner2"></span>
                            <i class="fa fa-quote-left"></i>
                          </span>                            
                        </div>  
                        <div class="dt-sc-margin15"></div>
                        
                        <!--hexagon-figure ends-->
                        <div class="dt-sc-testimonial-carousel-wrapper">
                          <div class="dt-sc-testimonial-carousel">


                            <div class="testimonial"> 

                              <h4>A man sooner or later discovers that he is the master-gardener of his soul, the director of his life. </h4>
                              
                              
                              <span>- James Allen</span>
                              
                            </div>
                            
                            <div class="testimonial"> 

                              <h4>A man sooner or later discovers that he is the master-gardener of his soul, the director of his life. </h4>
                              
                              
                              <span>- James Allen</span>
                              
                            </div>
                            
                            
                          </div>
                          <div class="carousel-arrows">


                          </div>
                        </div>
                      </div>
                      <!--container ends-->
                    </div>
                    <!--tweet-content ends-->
                  </div>
                  
                  <!--full-width-section ends-->
                </div>
                <style>

                 .full-width-quote{background:url(//cdn.shopify.com/s/files/1/0687/0339/files/quote-parallax-bg_1.jpg?v=1504165272) repeat-y 50% 0 fixed; }

               </style>

             </div><div id="shopify-section-1504165398787" class="shopify-section index-section"><div data-section-id="1504165398787" data-section-type="blog-section">

              <section  class="content inner-page blog" id="section_7">
                <div class="dt-sc-margin50"></div>
                <div class="dt-sc-margin10"></div>
                <div class="container">

                  <!--main-title starts-->
                  <div class="main-title">
                    <h2 class="align-center">OUR
                      <span class="wlast">BLOG</span>
                      <span class="small-line"></span></h2>
                    </div>
                    <!--main-title ends-->
                    
                    
                    <p class="align-center grey">Our Passion leads to design, design leads to performance, performance leads to success. We believe that apps and websites should not only be eye <br>catching but actually provide a great user experience that users will remember.</p>
                    <div class="dt-sc-margin30"></div>
                    
                  </div>
                  
                  <div class="content-main"> 
                    <div class="fullwidth-section"> 
                      <div class="container">
                        <div class="blog-items apply-isotope">


                          <?php $counter=0; ?>
                          @foreach($blogs as $blog)
                          @if($counter < 3)
                          <div class="dt-sc-one-third first column first">
                            <article class="blog-entry">

                              <div class="entry-thumb">
                                <a class="article__featured-image" href="/blog-detail/{{$blog->id}}">
                                  <img src="/uploads/{{$blog->f_image}}" alt="{{$blog->title}}" />
                                </a>
                              </div>      
                              
                              <div class="entry-details entry-move-right">
                                <div class="entry-title">

                                  <span class="hexagon">
                                    <span class="corner1"></span>
                                    <span class="corner2"></span>
                                    <i class="fa fa-pencil"></i>
                                  </span>
                                  
                                  <h4><a href="/blog-detail/{{$blog->id}}"> {{$blog->title}}</h4>
                                </div>
                                <div class="entry-body">
                                  <p><p>{{$blog->description}}...</p>
                                </div>
                                <div class="entry-metadata">

                                  <h6 class="date"><span class="hexagon2">
                                    <span class="corner1"></span>
                                    <span class="corner2"></span><i class="fa fa-calendar"></i></span> 
                                    {{$blog->created_at}}
                                  </h6>
                                  
                                  
                                  <h6 class="author"> <span class="hexagon2">
                                    <span class="corner1"></span>
                                    <span class="corner2"></span><i class="fa fa-user"></i></span> <a href="/blogs/news/18477647-alone-in-the-backyard" title="ram">Admin</a> 
                                  </h6>
                                  

                                  
                                </div>
                              </div>
                            </article>
                          </div>
                          @endif
                          <?php $counter++;?>
                          @endforeach
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="dt-sc-margin30"></div>
                  <div class="dt-sc-margin10"></div>
                  <div class="margin5"></div>
                  
                  <a class="dt-sc-button large" href="/blogs">View More</a>
                  <div class="dt-sc-margin70"></div>
                  <div class="margin5"></div>
                  
                  
                </section>
                
              </div>
            </div><div id="shopify-section-1504165684148" class="shopify-section index-section">
              <div data-section-id="1504165684148" data-section-type="tweet-section">

              </div>
              <style>
                .full-width-tweets{background:url(//cdn.shopify.com/s/files/1/0687/0339/files/tweets-bg_1.jpg?v=1504165746) repeat-y 50% 0 fixed; }
              </style>
            </div><div id="shopify-section-1504165911457" class="shopify-section index-section"><div data-section-id="1504165911457" data-section-type="contact-section">

              <!--contacts starts--> 
              <section class="contacts" id="section_8">
                <div class="dt-sc-margin50"></div>
                <div class="dt-sc-margin10"></div>
                <!--container starts-->
                <div class="container">
                  <!--main-title starts-->
                  
                  <div class="main-title">
                    <h2 class="align-center">CONTACT
                      <span class="wlast">US</span>
                      <span class="small-line"></span></h2>
                    </div>
                    
                    <!--main-title ends-->
                    
                    <p class="align-center grey">Our Passion leads to design, design leads to performance, performance leads to success. We believe that apps and websites should not only be eye catching but actually provide a great user experience <br>that users will remember.</p>
                    
                  </div>
                  <!--container ends-->
                  <div class="dt-sc-margin20"></div>
                  <div class="dt-sc-margin15"></div>
                  
                  
                  <div class="parallax full-width-contact">
                    <!--container starts-->
                    <div class="container">

                      <!--column one-half starts-->
                      <div class="page-with-contact-form column dt-sc-one-half">
                        <form method="post" action="/contact#contact_form" id="contact_form" accept-charset="UTF-8" class="contact-form"><input type="hidden" name="form_type" value="contact" /><input type="hidden" name="utf8" value="✓" />


                          <div id="contactFormWrapper" class="contact-form">
                            <input type="text" id="contactFormName" name="contact[name]" placeholder="Name" />
                            <p class="column dt-sc-one-half">
                              <input type="email" id="contactFormEmail" name="contact[email]" placeholder="Mail" />
                            </p> 
                            <p class="column dt-sc-one-half last">
                              <input type="text" id="contactFormTelephone" name="contact[phone]" placeholder="Phone" />
                            </p> 
                            <textarea rows="15" cols="75" id="contactFormMessage" name="contact[body]" placeholder="Message"></textarea>
                            <p>
                              <input type="submit" id="contactFormSubmit" value="Send" class="btn" />
                            </p>
                          </div>
                        </form>
                      </div>
                      
                      <!--column one-half ends-->
                      
                      <!--column one-half starts-->
                      <div class="column dt-sc-one-half last">
                        <!--contact-info box starts-->
                        <div class="dt-sc-contact-info">

                          <h5>Location
                            <span class="small-line"></span>
                          </h5>
                          
                          
                          <p><i class="fa fa-map-marker"></i>63-65 North Wharf Road, London, W2 1LA 
                            <br>
                          France</p>
                          
                          
                          <p><i class="fa fa-phone"></i>0422 - 3456 8734</p>
                          
                          
                          <p><i class="fa fa-envelope-o"></i>@yahoo.co</p>
                          <div class="dt-sc-margin15"></div>
                          
                          
                          <h5 >Business Hours
                            <span class="small-line"></span></h5>
                            
                            
                            <p class="hours"><i class="fa fa-clock-o"></i><span>Monday - Friday - 9am to 5pm <br> Saturday & Sunday - 9am to 1pm</span></p>
                            
                            
                            <p class="note">Note: All Commercial Holidays are Applicable</p>
                            
                            
                            <p class="customer-support">24 x 7 Live Customer Support Available</p>
                            <div class="dt-sc-margin15"></div>
                            
                            
                            <h5 class="margin25">Follow Us
                              <span class="small-line"></span></h5>
                              

                              
                              <ul class="dt-sc-social-icons">

                                <li><a href="#">
                                  <span class="hexagon">
                                    <span class="corner1"></span>
                                    <span class="corner2"></span>
                                    <i class="fa fa-facebook"></i>
                                  </span> </a></li>
                                  
                                  
                                  <li><a href="#">
                                    <span class="hexagon">
                                      <span class="corner1"></span>
                                      <span class="corner2"></span>
                                      <i class="fa fa-twitter"></i>
                                    </span> </a></li>
                                    
                                    
                                    <li><a href="">
                                      <span class="hexagon">
                                        <span class="corner1"></span>
                                        <span class="corner2"></span>
                                        <i class="fa fa-youtube"></i>
                                      </span> </a></li>
                                      
                                      
                                      <li><a href="#">
                                        <span class="hexagon">
                                          <span class="corner1"></span>
                                          <span class="corner2"></span>
                                          <i class="fa fa-linkedin"></i>
                                        </span> </a></li>
                                        
                                        
                                        <li><a href="#">
                                          <span class="hexagon">
                                            <span class="corner1"></span>
                                            <span class="corner2"></span>
                                            <i class="fa fa-google-plus"></i>
                                          </span> </a></li>
                                          

                                          
                                          
                                          
                                        </ul>
                                        
                                      </div>
                                      <!--contact-info box ends-->
                                    </div>
                                    
                                    <!--column one-half ends-->
                                  </div>
                                  <!--container ends-->
                                </div>
                                
                                <!--full-width-section ends-->
                              </section>
                              <!--contacts ends-->
                              
                            </div>




                            <style>
                              .full-width-contact{background:url(//cdn.shopify.com/s/files/1/0687/0339/files/contact-full-width_ad35324c-699d-4f64-8d9f-72380c349487.jpg?v=1504166146) repeat-y 50% 0 fixed; }
                              .dt-sc-contact-info h5{color:#003993;}

                              .dt-sc-contact-info h5{border-bottom:1px solid #4c4c4c;}

                              .dt-sc-contact-info .fa{color:#003993;}

                              .dt-sc-contact-info p{color:#ffffff;}

                            </style>
                          </div><!-- END content_for_index -->
                        </div>
                        <!--main-content ends-->
                        @include('layouts.footer')