@extends('layouts.app')

@section('content')

<div id="main-content">

  <div class="page-title page-title-bg  subtitle-for-single-product"> <div class="page-title-container">
    <h2 class="align-center">
      
      <span class="wlast">{{$product->name}}</span>
      <span class="small-line"></span></h2>
  </div>
</div>
<div class="dt-sc-margin70">
  <div class="message_box">
 @if (count($errors) > 0)
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
          @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
          </div>
          @endif
</div>
</div>
<div id="product" class="flying-ninja container">
  <section id="primary" class="content-full-width">
    <!-- Begin product photos -->
    <div class="images">
      <!-- Begin featured image -->
      <div class="image featured">
        <a href="" class="zoom" id="placeholder">
          <img src="/uploads/{{$product->image1}}" alt="{{$product->name}}" />
        </a>
      </div>
      <!-- End product image -->
      <div class="thumbnails">

        <!-- Begin thumbnails -->
        <div class="thumbs clearfix">
          <ul>
            <li class="image ">
              <a href="/uploads/{{$product->image1}}" data-original-image="/uploads/{{$product->image1}}">
                <img src="/uploads/{{$product->image1}}" alt="{{$product->name}}" />
              </a>
            </li>
            <li class="image ">
              <a href="/uploads/{{$product->image2}}" data-original-image="/uploads/{{$product->image2}}">
                <img src="/uploads/{{$product->image2}}" alt="{{$product->name}}" />
              </a>
            </li>


          </ul>
        </div>
        <!-- End thumbnails -->


      </div>
    </div>
    <!-- End product photos -->
    <!-- Begin description -->
    <div class="summary entry-summary product-details-hover">


      <p class="price" id="price-preview">
        <span class="amount"><del>Rs.{{$product->price}}</del> &nbsp;Rs.{{$product->sale_price}}</span></p>

          <form id="add-item-form" action="/product-detail/{{$product->id}}" method="POST" class="variants clearfix">
            @csrf
            <!-- Begin product options -->
            <div class="product-options ">

              <div class="select clearfix">
              </div>
              <div class="selector-wrapper product-set">
                <label> Product Type: </label>
                <p>Momos</p>
              </div>
              <div class="selector-wrapper product-set">
                <label> Product Vendor: </label>
                <p>Chini Group</p>
              </div>

              <div class="selector-wrapper product-quantity">
                <label class="qty">Quantity</label>
                <input class="qtyminus" type="button" value="-">
                <input id="quantity" type="number" name="quantity" value="1"  min="1"  class="tc item-quantity" />
                <input class="qtyplus" type="button" value="+">
              </div>
              
            </div>
            <div class="add-wishlist">
            <div class="add-wishlist-button show">

              <!-- <a class="add-to-wishlist dt-sc-button"  href="/customer-info/{{$product->id}}" title="Add to Wishlist">Buy Product</a> -->
              <button class="add-to-wishlist dt-sc-button">Buy Product</button>


            </div>
          </div>
            <!-- End product options -->
          </form>
          
          <!-- Begin social buttons -->

          <div class="social">





            <div class="social-sharing is-clean" data-permalink="https://citrus-demo.myshopify.com/products/flying-ninja">


              <a target="_blank" href="http://www.facebook.com/sharer.php?u=https://citrus-demo.myshopify.com/products/flying-ninja" class="share-facebook">
                <span class="fa fa-facebook"></span>
                <span class="share-title">Share</span>

              </a>



              <a target="_blank" href="http://twitter.com/share?url=https://citrus-demo.myshopify.com/products/flying-ninja" class="share-twitter">
                <span class="fa fa-twitter"></span>
                <span class="share-title">Tweet</span>

              </a>




              <a target="_blank" href="http://pinterest.com/pin/create/button/?url=https://citrus-demo.myshopify.com/products/flying-ninja" class="share-pinterest">
                <span class="fa fa-pinterest"></span>
                <span class="share-title">Pin It</span>

              </a>



              <a target="_blank" href="http://www.thefancy.com/fancyit?ItemURL=https://citrus-demo.myshopify.com/products/flying-ninja" class="share-fancy">
                <span class="fa fa-trello"></span>
                <span class="share-title">Fancy</span>
              </a>



              <a target="_blank" href="http://plus.google.com/share?url=https://citrus-demo.myshopify.com/products/flying-ninja" class="share-google">
                <!-- Cannot get Google+ share count with JS yet -->
                <span class="fa fa-google"></span>

                <span class="share-count is-loaded">+1</span>

              </a>


            </div>

          </div>

          <!-- End social buttons -->
        </div>
        <!-- End description -->
        <div class="dt-sc-tabs-container">
          <ul class="dt-sc-tabs">
            <li class="description_tab active">
              <a href="#tab-description">Description</a>
            </li>

            <!-- <li class="reviews_tab">
              <a href="#tab-reviews">Reviews</a>
            </li> -->

          </ul>
          <div id="tab-description" class="panel dt-sc-tabs-content entry-content">
            <p> <span style="color: #2b2b2b; font-family: 'Source Sans Pro', sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #ffffff;"><?php echo ($product->description)?></span> </p>
          </div>
        </div>
      </section>
      <!-- Begin related product -->

    </div>


  </div> 
  <div class="dt-sc-margin70"></div>


</div>
<!--main-content ends-->
@endsection