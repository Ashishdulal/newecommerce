@extends('layouts.app')

@section('content')

<div class="page-title page-title-bg  subtitle-for-single-product"> <div class="page-title-container">    

  <!--main-title starts-->
  <div class="main-title">
    <h2 class="align-center">
      OUR
      <span class="wlast">Products</span>
      <span class="small-line"></span></h2>
    </div>
    <!--main-title ends-->
  </div>
</div>

<div id="shopify-section-1504162990874" class="shopify-section index-section"><div data-section-id="1504162990874" data-section-type="portfolio-section">

  <section class="portfolio" id="section_4">
    <div class="dt-sc-margin50"></div>
    <div class="container">
      <!--sorting-container starts-->
      <div class="sorting-container">
        <a data-filter=".all-sort" class="" href="#">All</a> 
        @foreach($product_categories as $productcat)
        <a data-filter=".{{ $productcat->id }}" id="{{ $productcat->id }}" href="#" class="">{{$productcat->title}}</a>
        @endforeach      
      </div>
      <!--sorting-container ends-->
      <div class="dt-sc-margin20">    <aside class="widget widget_product_search" >


        <div class="search-field fl">
          <form class="search" id="searchform" action="/search">
            <input type="text" placeholder="Search" name="q" class="search_box" placeholder="Search" value=""  />
            <input type="submit" value="Search" id="searchsubmit">
            <input type="hidden" value="product" name="post_type">
          </form>    
        </div>
      </aside></div>
    </div>
    <!--container ends-->
    <!--portfolio-container starts-->
    <div class="portfolio-container isotope" style="position: relative; overflow: hidden; height: 475px;">
      <!--gallery-no-space starts-->
      @foreach($product_categories as $productcat)
      @foreach($products as $product)
      @if($productcat->id === $product->cat_id)
      <div class="column dt-sc-one-fourth gallery no-space new games {{ $productcat->id }} indoor  landscape  outdoor  portfolio  all-sort isotope-item" id="productFilter" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">

        <figure> 
          <img src="/uploads/{{$product->image1}}" title="" alt="image">
          <!--image-overlay starts-->
          <div class="image-overlay">
            <span class="white-box"></span>
            <div class="image-overlay-text">
              <h4><a href="/product-detail/{{$product->id}}">{{$product->name}}</a></h4> 
              <span class="small-line"></span>
              <p><a href="/product-detail/{{$product->id}}">{{$productcat->title}}</a></p>
              <span class="price"><del>Rs.{{$product->price}}</del>&nbsp;<span class="amount">Rs.{{$product->sale_price}} </span></span>
              <ul class="links">               
                <li><a href="/uploads/{{$product->image1}}"><span class="hexagon"><span class="corner1"></span><span class="corner2"></span><i class="fa fa-search"></i></span></a></li>
                
                
                <li><a href="/product-detail/{{$product->id}}" class="link"><span class="hexagon"><span class="corner1"></span><span class="corner2"></span><i class="fa fa-external-link"></i></span></a></li>
                
              </ul>
            </div>
            <span class="border-line"></span>
          </div>
          <!--image-overlay ends-->
        </figure>       

      </div>
      @endif
      @endforeach
      @endforeach
      <!--gallery-no-space ends-->
    </div>
    <!-- portfolio-container ends-->
  </section>
</div>
</div>

@endsection