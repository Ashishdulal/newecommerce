jQuery.noConflict();
jQuery(document).ready(function($){
 /*Portfolio Carousel*/
  if(jQuery(".portfolio-carousel-wrapper").length) {
    jQuery('.portfolio-carousel').carouFredSel({
      responsive: true,
      auto: false,
      width: '100%',
      prev: '.portfolio-prev-arrow',
      next: '.portfolio-next-arrow',
      height: 420,
      scroll: 1,				
      items: {
        width: 230,
        visible: {
          min: 1,
          max: 4
        }
      }				
    });			
  }

  "use strict";

  $('.qtyplus').click(function(e){
    e.preventDefault();
    var currentVal = parseInt($('input[name="quantity"]').val());
    if (!isNaN(currentVal)) {
      $('input[name="quantity"]').val(currentVal + 1);
    } else {
      $('input[name="quantity"]').val(1);
    }
  });

  $(".qtyminus").click(function(e) {

    e.preventDefault();
    var currentVal = parseInt($('input[name="quantity"]').val());
    if (!isNaN(currentVal) && currentVal > 0) {
      $('input[name="quantity"]').val(currentVal - 1);
    } else {
      $('input[name="quantity"]').val(1);
    }
  });
  
  
   $('.qtyplus_cart').click(function(e){
    e.preventDefault();
     var currentVal = parseInt($(this).parent().find('input[name="updates[]"]').val());
    if (!isNaN(currentVal)) {
      $(this).parent().find('input[name="updates[]"]').val(currentVal + 1);
    } else {
      $(this).parent().find('input[name="updates[]"]').val(1);
    }
  });

  $(".qtyminus_cart").click(function(e) {

    e.preventDefault();
    var currentVal = parseInt($(this).parent().find('input[name="updates[]"]').val());
    if (!isNaN(currentVal) && currentVal > 1) {
      $(this).parent().find('input[name="updates[]"]').val(currentVal - 1);
    } else {
      $(this).parent().find('input[name="updates[]"]').val(1);
    }
  });
  
  
  
  $(".dt-menu-expand").click(function(event){
    event.preventDefault();
    if( $(this).hasClass("dt-mean-clicked") ){
      $(this).text("+");
      if( $(this).prev('ul').length ) {
        $(this).prev('ul').slideUp(400);
      } else {
        $(this).prev('.megamenu-child-container').find('ul:first').slideUp(600);
      }
    } else {
      $(this).text("-");
      if( $(this).prev('ul').length ) {
        $(this).prev('ul').slideDown(400);
      } else{
        $(this).prev('.megamenu-child-container').find('ul:first').slideDown(2000);
      }
    }

    $(this).toggleClass("dt-mean-clicked");
    return false;
  });


  if($('ul.dt-sc-tabs').length > 0) {
    $('ul.dt-sc-tabs').tabs('> .dt-sc-tabs-content');
  }

  /* Sticky Header */
  $("#header").sticky({ topSpacing: 0 });
  $('.more').css({"cursor":"pointer"});
  $('.blogmore').css({"cursor":"pointer"});
 
  
  
 
/* Mean Menu for Mobile */
  $('nav#main-menu').meanmenu({
    meanMenuContainer :  $('#menu-container'),
    meanRevealPosition:  'right',
    meanScreenWidth   :  767
  });



  /* Goto Top */
  $().UItoTop({ easingType: 'easeOutQuart' });

  /* bxSlider */
  if( $('.slider').length ) {
    $('.slider').bxSlider({
      mode: 'fade',
      auto: true,
      infiniteLoop: true,
      hideControlOnEnd: true,
      pause: 3000
    });
  }

  //Portfolio Single page Slider
  if( ($(".portfolio-slider").length) && ($(".portfolio-slider li").length > 1) ) {
    $('.portfolio-slider').bxSlider({ 
      auto:false, useCSS:false, autoHover:true, adaptiveHeight:true });
  }//Portfolio Single page Slider


  /* Parallax Section */
  $('.parallax').each(function(){
    $(this).bind('inview', function (event, visible) {
      if(visible == true) {
        $(this).parallax("50%", 0.3);
      } else {
        $(this).css('background-position','');
      }
    });
  });

  /* PrettyPhoto For Portfolio */
  if($(".gallery").length) {
    $(".gallery a[data-gal^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square',slideshow:3000, autoplay_slideshow: false,social_tools: false,deeplinking:false});		
  }

  /* Donut Chart */
  $('.dt-sc-donutchart1').one('inview', function (event, visible){
    if (visible == true) {
      $(".dt-sc-donutchart1").donutchart({'size': 200, 'donutwidth': 2, 'fgColor': '#fe6b35', 'bgColor': '#f5f5f5', 'textsize': 30 });
      $(".dt-sc-donutchart1").donutchart("animate");

      $(".dt-sc-donutchart2").donutchart({'size': 200, 'donutwidth': 2, 'fgColor': '#665de5', 'bgColor': '#f5f5f5', 'textsize': 30 });
      $(".dt-sc-donutchart2").donutchart("animate");

      $(".dt-sc-donutchart3").donutchart({'size': 200, 'donutwidth': 2, 'fgColor': '#36a6a0', 'bgColor': '#f5f5f5', 'textsize': 30 });
      $(".dt-sc-donutchart3").donutchart("animate");

      $(".dt-sc-donutchart4").donutchart({'size': 200, 'donutwidth': 2, 'fgColor': '#f4d30f', 'bgColor': '#f5f5f5', 'textsize': 30 });
      $(".dt-sc-donutchart4").donutchart("animate");
    }
  });

  /*Testimonial  Carousel*/
  if($(".dt-sc-testimonial-carousel").length) {
    $('.dt-sc-testimonial-carousel').carouFredSel({
      responsive: true,
      auto: false,
      width: '100%',
      height: 'variable',
      prev: '.testimonial-prev',
      next: '.testimonial-next',
      scroll: 1,
      items: { height: 'variable', visible: { min: 1,max: 2} }
    });
  }

  //Smart Resize Start
  $(window).smartresize(function(){
    /* Blog Template Isotope */
    if( $(".apply-isotope").length ){
      $(".apply-isotope").each(function(){
        $(this).isotope({itemSelector : '.column',transformsEnabled:false,masonry: { gutterWidth: 25} });
      });
    }

    if( $('.portfolio-container').length ){
      var $width = $('.portfolio-container').hasClass("no-space") ? 0 : 0;
      $('.portfolio-container').css({overflow:'hidden'}).isotope({itemSelector : '.column', masonry: { gutterWidth: $width } });
    }
  });//Smart Resize End

  //Window Load Start
  $(window).load(function(){
    /* Blog Template Isotope */
    if( $(".apply-isotope").length ){
      $(".apply-isotope").each(function(){
        $(this).isotope({itemSelector : '.column',transformsEnabled:false,masonry: { gutterWidth: 25} });
      });
    }

    if( $('.portfolio-container').length ){
      var $width = $('.portfolio-container').hasClass("no-space") ? 0 : 0;
      $('.portfolio-container').isotope({
        filter: '*',
        masonry: { gutterWidth: $width },
        animationOptions: { duration: 750, easing: 'linear', queue: false  }
      });
    }

    if($("div.sorting-container").length){
      $("div.sorting-container a").click(function(){
        $("div.sorting-container a").removeClass("active-all");
        var $width = $('.portfolio-container').hasClass("no-space") ? 0 : 0;
        var selector = $(this).attr('data-filter');
        $(this).addClass("active-all");

        $('.portfolio-container').isotope({
          filter: selector,
          masonry: { gutterWidth: $width },
          animationOptions: { duration: 750, easing: 'linear', queue: false  }
        });
        return false;
      });
    }

   
  });//Window Load End

 

  //Animation
  $(".animate").each(function(){
    $(this).bind('inview', function (event, visible) {
      var $this = $(this),
          $animation = ( $this.data("animation") !== undefined ) ? $this.data("animation") : "slideUp";
      $delay = ( $this.data("delay") !== undefined ) ? $this.data("delay") : 300;

      if (visible == true) {
        setTimeout(function() { $this.addClass($animation);	},$delay);
      }else{
        setTimeout(function() { $this.removeClass($animation); },$delay);
      }
    });
  });

  //Related Post Carousel
  if( $("#mycarousel").length ){

    //Related Post
    $("#mycarousel").jcarousel({
      scroll: 1,
      initCallback: mycarousel_initCallback,
      buttonNextHTML: null,
      buttonPrevHTML: null
    });
  }//Related Post Carousel

  $('form[name="contact-form"]').submit(function(){
    var $form = $(this),
        $msg = $(this).prev('div.message'),
        $action = $form.attr('action');

    $.post($action,$form.serialize(),function(data){
      $form.fadeOut("fast", function(){ $msg.hide().html(data).show('fast'); });
    });
    return false;
  });

  //Animate Number...
  $('.dt-sc-num-count').each(function(){
    $(this).one('inview', function (event, visible) {
      if(visible === true) {
        var val = $(this).attr('data-value');
        $(this).animateNumber({ number: val	}, 2000);
      }
    });
  });


  // Portfolio load more
  var portfolio_track_click = 0;
  $('.more').click(function(){
    if(portfolio_track_click == 0) 
    {
      $('.more').html('Loading...');
      $.ajax({
        type: "POST",
        url: "portfolio_load_more.html",
        dataType: "html",
        cache: false,
        msg : '',
        success: function(msg){
          $('.portfolio-container').append(msg);
          $(window).trigger( 'resize' );
          $('.portfolio-container').isotope( 'reloadItems' ).isotope();
          $("a[data-gal^='prettyPhoto[gallery]']").prettyPhoto({
            show_title: false,
            social_tools: false,
            deeplinking: false
          });
          $('.more').text('Load More');
          portfolio_track_click++;
        }
      });
    }
    if(portfolio_track_click > 0){
      $('.more').text('No more Posts to show').css({"cursor":"default"});
    }
  });

  // Blog load more
  var blog_track_click = 0;	
  $('.blogmore').click(function(){
    if(blog_track_click == 0) 
    {
      $('.blogmore').html('Loading...');
      $.ajax({
        type: "POST",
        url: "blog_load_more.html",
        dataType: "html",
        cache: false,
        success: function(msg){
          $('.blog-items').append(msg);
          $('.blog-items').isotope( 'reloadItems' ).isotope();
          $('.blogmore').text('Load More');
          blog_track_click++;
        }
      });
    }
    if(blog_track_click > 0){
      $('.blogmore').text('No more Posts to show').css({"cursor":"default"});
    }
  });

});


(function($) {
  $(function() {

    "use strict";

    var jcarousel = $('.jcarousel');

    jcarousel
    .on('jcarousel:reload jcarousel:create', function () {
      var width = jcarousel.innerWidth();

      if (width >= 900) {
        width = width / 6;
      }else if (width >= 350) {
        width = width / 2;
      }

      jcarousel.jcarousel('items').css('width', width + 'px');
    })
    .jcarousel({
      wrap: 'circular'
    });

    $('.jcarousel-control-prev').jcarouselControl({
      target: '-=1'
    });

    $('.jcarousel-control-next').jcarouselControl({
      target: '+=1'
    });

    $('.jcarousel-pagination').on('jcarouselpagination:active', 'a', function() {
      $(this).addClass('active');
    })
    .on('jcarouselpagination:inactive', 'a', function() {
      $(this).removeClass('active');
    })
    .on('click', function(e) {
      e.preventDefault();
    })
    .jcarouselPagination({
      perPage: 1,
      item: function(page) {
        return '<a href="#' + page + '">' + page + '</a>';
      }
    });
  });
})(jQuery);

(jQuery);